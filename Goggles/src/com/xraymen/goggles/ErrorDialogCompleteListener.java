package com.xraymen.goggles;

import com.xraymen.goggles.commands.AsyncCommandBase.CompleteListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;

/**
 * Show a progress dialog while posting. Show a dialog on error and finish the activity on success. 
 * @author amitlissack
 *
 */
public class ErrorDialogCompleteListener implements CompleteListener {

    private Activity _activity = null;
    private ProgressDialog _progressDialog = null;
    
    public ErrorDialogCompleteListener(Activity activity) {
        _activity = activity;
        _progressDialog = ProgressDialog.show(_activity, _activity.getString(R.string.app_name), _activity.getString(R.string.please_wait));
    }

    @Override
    public void onSuccess() {
        _progressDialog.cancel();
        _activity.finish();
    }

    @Override
    public void onFailure(final String error) {
        _activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _progressDialog.cancel();
                AlertDialog d = new AlertDialog.Builder(_activity).create();
                d.setMessage(error);
                d.setTitle(R.string.app_name);
                d.show();
            }
        });
    }
}
