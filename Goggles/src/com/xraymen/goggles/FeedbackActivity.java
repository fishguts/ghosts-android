package com.xraymen.goggles;

import com.xraymen.goggles.actionbar.ActionBarActivity;
import com.xraymen.goggles.commands.SendFeedbackCommand;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class FeedbackActivity extends ActionBarActivity {

    public FeedbackActivity() {
        super(R.layout.titlebar_post, R.id.titlebar);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feedback);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;
        switch (item.getItemId()) {
        case (R.id.action_post):
            post();
            break;
        case (R.id.action_home):
            cancel();
            break;
        default:
            handled = false;
            break;
        }
        return handled;
    }

    /**
     * 
     */
    public void cancel() {
        finish();
    }

    /**
     * 
     */
    public void post() {
        EditText feedbackTextControl = (EditText)findViewById(R.id.editTextFeedback);
        String feedbackText = feedbackTextControl.getText().toString().trim();

        if (TextUtils.isEmpty(feedbackText)) {
            feedbackTextControl.setError(getString(R.string.error_missing_field));
            return;
        }

        SendFeedbackCommand command = new SendFeedbackCommand(this, feedbackText, new ErrorDialogCompleteListener(this));
        command.execute();
    }
}
