package com.xraymen.goggles;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.xraymen.goggles.logging.LoggingSetup;

import android.app.Application;
import android.content.Context;

public class GogglesApplication extends Application {
    
    private static final Logger LOGGER = Logger.getLogger(GogglesApplication.class.getName());
    
    private static GogglesApplication _instance = null;
    
    private boolean _loggedIn = false;
    
    private boolean _introCompleted = false;
    
    private boolean _locationProvidersChecked = false;
    
    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        
        LoggingSetup.setup();

        LOGGER.log(Level.INFO, "Application created");
    }

    /**
     * Access to the single application instance
     * @return the application
     */
    public static GogglesApplication getApplication() {
        return _instance;
    }

    /**
     * Get the application context
     * @return application context
     */
    public static Context getContext() {
        return _instance.getApplicationContext();
    }

    /**
     * 
     * @return
     */
    public boolean isLoggedIn() {
        return _loggedIn;
    }

    /**
     * 
     * @param loggedIn
     */
    public void setLoggedIn(boolean loggedIn) {
        _loggedIn = loggedIn;
    }

    /**
     * 
     * @return
     */
    public boolean isIntroCompleted() {
        return _introCompleted;
    }

    /**
     * 
     * @param introCompleted
     */
    public void setIntroCompleted(boolean introCompleted) {
        this._introCompleted = introCompleted;
    }

    /**
     * 
     * @return
     */
    public boolean isLocationProvidersChecked() {
        return _locationProvidersChecked;
    }

    /**
     * 
     * @param locationProvidersChecked
     */
    public void setLocationProvidersChecked(boolean locationProvidersChecked) {
        this._locationProvidersChecked = locationProvidersChecked;
    }
}
