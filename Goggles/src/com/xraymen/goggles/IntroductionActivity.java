package com.xraymen.goggles;

import java.util.logging.Level;
import java.util.logging.Logger;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

public class IntroductionActivity extends DialogFragment implements OnTouchListener {

    private static final Logger LOGGER = Logger.getLogger(IntroductionActivity.class.getName());

    private static final float SKIP_x1 = 0.665f, SKIP_y1 = 0.00419610472554215f;
    private static final float SKIP_x2 = 0.96625f, SKIP_y2 = 0.0533396735832427f;

    private static final float NEVER_AGAIN_x1 = 0.7025f, NEVER_AGAIN_y1 = 0.958814903831806f;
    private static final float NEVER_AGAIN_x2 = 0.97875f, NEVER_AGAIN_y2 = 0.996418687289722f;

    private static final float SHOW_AGAIN_x1 = 0.40375f, SHOW_AGAIN_y1 = 0.958814903831806f;
    private static final float SHOW_AGAIN_x2 = 0.68f, SHOW_AGAIN_y2 = 0.996418687289722f;

    private static final RectF SKIP_RECT = new RectF(SKIP_x1, SKIP_y1, SKIP_x2, SKIP_y2);
    private static final RectF NEVER_AGAIN_RECT = new RectF(NEVER_AGAIN_x1, NEVER_AGAIN_y1, NEVER_AGAIN_x2, NEVER_AGAIN_y2);
    private static final RectF SHOW_AGAIN_RECT = new RectF(SHOW_AGAIN_x1, SHOW_AGAIN_y1, SHOW_AGAIN_x2, SHOW_AGAIN_y2);

    /**
     * 
     * @author amitlissack
     *
     */
    private enum HotSpot {
        SKIP(SKIP_RECT), 
        NEVER_AGAIN(NEVER_AGAIN_RECT), 
        SHOW_AGAIN(SHOW_AGAIN_RECT);

        private final RectF _rect;

        private HotSpot(final RectF rect) {
            _rect = rect;
        }

        public boolean isInside(float x, float y) {
            if (_rect != null) {
                return _rect.contains(x, y);
            }
            return false;
        }
    };

    private ViewGroup _imageViewContatiner = null;

    private HotSpot _touchedHotSpot = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        
        Dialog d = new Dialog(getActivity(), R.style.SplashScreen);
        
        try {
            
            d.setContentView(R.layout.activity_introduction);
            
            _imageViewContatiner = (ViewGroup)d.findViewById(R.id.imageContainer);
            _imageViewContatiner.setOnTouchListener(this);
            
            addImageView(R.drawable.image_intro_upper);
            addImageView(R.drawable.image_intro_lower);

        } catch (Throwable e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            saveShowIntroFlag(false);
            dismiss();
        }
        return d;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();

        GogglesApplication.getApplication().setIntroCompleted(true);
    }

    /**
     * @param drawableId
     */
    private void addImageView(int drawableId) {
        //Create an image view for the intro image
        ImageView imageView = new ImageView(_imageViewContatiner.getContext());
        
        //We will load the intro image and scale it to fit the full screen.
         
        //Get the introduction image bitmap 
        Bitmap originalBitmap = BitmapFactory.decodeResource(getResources(), drawableId);
        
        int w = originalBitmap.getWidth();
        int h = originalBitmap.getHeight();
         
        //Width of the screen.  
        int windowWidth = getResources().getDisplayMetrics().widthPixels;
         
        //Scale factor
        float scale = (float) windowWidth / (float) w;
        
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        
        //Set up the image view to scale image to fit the screen
        imageView.setImageMatrix(matrix);
        imageView.setScaleType(ScaleType.MATRIX);
        imageView.setImageDrawable(new BitmapDrawable(getResources(), originalBitmap));
        imageView.setLayoutParams(new LinearLayout.LayoutParams((int)(w * scale), (int)(h * scale)));
         
        LOGGER.info(String.format("Original dimensions=%d, %d Window width=%d Scale=%f Scaled dimensions=%d, %d", w, h, windowWidth, scale, (int)(w * scale), (int)(h * scale)));
         
        _imageViewContatiner.addView(imageView);
    }

    /**
     * 
     * @param hotspot
     */
    private void onHotSpotClicked(HotSpot hotspot) {

        switch (hotspot) {
        case NEVER_AGAIN:
        case SKIP:
            saveShowIntroFlag(false);
            dismiss();
            break;
        case SHOW_AGAIN:
            saveShowIntroFlag(true);
            dismiss();
            break;
        default:
            break;
        }
    }
    
    /**
     * 
     * @param showAgain
     */
    private void saveShowIntroFlag(boolean showAgain) {
        Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
        editor.putBoolean(getString(R.string.prefs_showIntroductionKey), showAgain);
        editor.commit();
    }

    @Override
    public boolean onTouch(View v, MotionEvent m) {

        if (m.getAction() == MotionEvent.ACTION_DOWN) {

            float x = m.getX() / _imageViewContatiner.getWidth();
            float y = m.getY() / _imageViewContatiner.getHeight();
            
            _touchedHotSpot = null;
            for (HotSpot spot : HotSpot.values()) {
                if (spot.isInside(x, y)) {
                    _touchedHotSpot = spot;
                    LOGGER.info("Down on HotSpot " + _touchedHotSpot.toString());
                    break;
                }
            }

        } else if (m.getAction() == MotionEvent.ACTION_UP) {

            float x = m.getX() / _imageViewContatiner.getWidth();
            float y = m.getY() / _imageViewContatiner.getHeight();

            if (_touchedHotSpot != null && _touchedHotSpot.isInside(x, y)) {
                LOGGER.info("Up on HotSpot " + _touchedHotSpot.toString());
                onHotSpotClicked(_touchedHotSpot);
            }
        }
        return true;
    }
}
