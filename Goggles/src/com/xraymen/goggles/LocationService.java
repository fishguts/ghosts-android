package com.xraymen.goggles;

import java.util.List;
import java.util.logging.Logger;

import com.xraymen.goggles.db.Settings;
import com.xraymen.goggles.db.SettingsTableHelper;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

public class LocationService extends Service implements LocationListener {

    private static final Logger LOGGER = Logger.getLogger(LocationService.class.getName());
    
    public static final String EXTRA_LOCATION = "location";

    private static final long SIGNIFICANT_TIME_DELTA = 1000 * 60 * 2;
    
    private static final int SIGNIFICANT_ACCURACY_DELTA = 100;
    
    private boolean _running = false;
    
    private Settings _settings = null;
    
    private final LocationServiceBinder _binder = new LocationServiceBinder();
    
    private Location _location = null;
    
    @Override
    public void onLocationChanged(Location location) {
        LOGGER.info(String.format("Location changed: %s", location.toString()));
        
        if (isBetterLocation(location, _location)) {
            //This location is better than cached one, save it and notify listeners.
            _location = location;
            fireUpdate(_location);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        LOGGER.info(String.format("Provider disabled:%s", provider));
    }

    @Override
    public void onProviderEnabled(String provider) {
        LOGGER.info(String.format("Provider enabled:%s", provider));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        LOGGER.info(String.format("Status changed Provider:%s Status:%d", provider, status));
    }

    @Override
    public IBinder onBind(Intent intent) {
        start();
        return _binder;
    }
    
    @Override
    public boolean onUnbind(Intent intent) {
        //All activities have unbound so stop
        stop();
        return true;
    }
    
    @Override
    public void onRebind(Intent intent) {
        //An activity is binding after after all others have unbound so start listening.
        start();
    }
    
    @Override
    public void onCreate() {
        LOGGER.info("onCreate");
    }
    
    @Override
    public void onDestroy() {
        LOGGER.info("onDestroy");
    }
    
    /**
     * 
     * @return
     */
    public Location getCurrentLocation() {
        return _location;
    }
    
    /**
     * Start listening for locations.
     */
    private void start() {
        LOGGER.info("start _running=" + Boolean.toString(_running));
        
        if (_running) {
            return;
        }
        _running = true;
        
        SettingsTableHelper tableHelper = new SettingsTableHelper();
        _settings = tableHelper.getSettings();
        
        LocationManager lm = (LocationManager)getSystemService(LOCATION_SERVICE);
        List<String> providers = lm.getProviders(false);
        
        //We will try to find if there's a better location than the last location we used
        Location bestLastKnownLocation = _location;

        for (String provider : providers) {
            boolean enabled = lm.isProviderEnabled(provider);
            lm.requestLocationUpdates(provider, _settings.getQueryIntervalMin() * 1000, 0, this);
            
            Location location = lm.getLastKnownLocation(provider);
            if (location != null && isBetterLocation(location, bestLastKnownLocation)) {
                bestLastKnownLocation = location;
            }
            LOGGER.info("Request location updates on: " + provider + " Enabled: " + Boolean.toString(enabled));
        }

        //Check if we have a better new location than the last location we used. 
        if (bestLastKnownLocation != null && !bestLastKnownLocation.equals(_location)) {
            LOGGER.info("Using initial location: " + bestLastKnownLocation.toString());
            _location = bestLastKnownLocation;
            fireUpdate(_location);
        }
    }
    
    /**
     * Stop listening for locations
     */
    private void stop() {
        
        LOGGER.info("stop _running=" + Boolean.toString(_running));
        
        if (!_running) {
            return;
        }
        _running = false;
        
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        lm.removeUpdates(this);
    }

    /**
     * Return true if newLocation is a better sample than oldLocation
     * @param newLocation
     * @param oldLocation
     * @return
     */
    public boolean isBetterLocation(Location newLocation, Location oldLocation) {
        if (oldLocation == null) {
            return true;
        }

        long timeDelta = newLocation.getTime() - oldLocation.getTime();
        boolean isNewer = timeDelta > 0;

        if (timeDelta < -SIGNIFICANT_TIME_DELTA) {
            //Much older. Ignore it!
            LOGGER.info("Ignoring location due to oldness");
            return false;
        }
        
        boolean isSameProvider = isSameProvider(newLocation.getProvider(), oldLocation.getProvider());
        if (isSameProvider && !isNewer) {
            //From same provider as cached but not newer. Ignore it.
            LOGGER.info("Ignoring location because it's from same provider but isn't newer");
            return false;
        }

        float accuracyDelta = newLocation.getAccuracy() - oldLocation.getAccuracy();
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMuchMoreAccurate = accuracyDelta < -SIGNIFICANT_ACCURACY_DELTA;
        boolean isMuchLessAccurate = accuracyDelta > SIGNIFICANT_ACCURACY_DELTA;

        if (isMuchMoreAccurate) {
            //More accurate. Use it. 
            LOGGER.info("Using much more accurate location");
            //Use a much more accurate location without checking if there's a significant distance change.  
            return true;
        } else if (timeDelta > SIGNIFICANT_TIME_DELTA) {
            //Much newer
            LOGGER.info("Accepting much newer location");
        } else if (isMoreAccurate) {
            //More accurate. 
            LOGGER.info("Accepting more accurate location");
        } else if (isNewer && !isLessAccurate) {
            //Newer and not less accurate.
            LOGGER.info("Accepting newer and not less accurate location");
        } else if (isNewer && !isMuchLessAccurate && isSameProvider) {
            //Newer, not much less accurate and the same provider as the last good sample.
            LOGGER.info("Accepting newer and not much less accurate location from same provider");
        } else {
            //Ignore everything which doesn't pass above.
            LOGGER.info("Ignoring new location");
            return false;
        }

        //Check if there's a significant change from last location
        return isSignificantChange(newLocation, oldLocation);
    }

    /**
     * Check difference in distance between the two locations to see if there's a significant change according to settings
     * @param newLocation
     * @param oldLocation
     * @return
     */
    private boolean isSignificantChange(final Location newLocation, final Location oldLocation) {

        if (_settings != null) {

            final double groundDifference = newLocation.distanceTo(oldLocation);
            final double heightDifference = (newLocation.hasAltitude() && oldLocation.hasAltitude()) ? Math.abs(newLocation.getAltitude() - oldLocation.getAltitude()) : 0;

            LOGGER.info(String.format("groundDifference = %f heightDifference = %f", groundDifference, heightDifference));

            if (groundDifference >= _settings.getRadiusHorizontal() || heightDifference >= _settings.getRadiusVertical()) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Compare providers
     * 
     * @param provider1
     * @param provider2
     * @return true if the same
     */
    static private final boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }    

    /**
     * 
     * @param location
     */
    private void fireUpdate(Location location) {
        Intent intent = new Intent(Notifications.LOCATION_UPDATE);
        intent.putExtra(EXTRA_LOCATION, location);
        Notifications.send(intent,  this);
    }

    /**
     * 
     * @author Amit
     *
     */
    public class LocationServiceBinder extends Binder {
        public Location getCurrentLocation() {
            return LocationService.this.getCurrentLocation();
        }
    }
}
