package com.xraymen.goggles;

import com.xraymen.goggles.actionbar.ActionBarActivity;
import com.xraymen.goggles.commands.LoginCommand;
import com.xraymen.goggles.db.Profile;
import com.xraymen.goggles.db.ProfileTableHelper;

import android.os.Bundle;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends ActionBarActivity {

    public LoginActivity() {
        super(R.layout.titlebar_login, R.id.titlebar);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_login);

        EditText textUsername = (EditText)findViewById(R.id.editTextUserName);
        EditText textPassword = (EditText)findViewById(R.id.editTextPassword);

        ProfileTableHelper profiles = new ProfileTableHelper();
        Profile profile = profiles.getLoginProfile();
        if (profile != null) {
            textUsername.setText(profile.getUsername());
            textPassword.setText(profile.getPassword());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;
        switch(item.getItemId()) {
        case R.id.action_login:
            login();
            break;
        case R.id.action_home:
            cancel();
            break;
        default:
            handled = false;
            break;
        
        }
        return handled;
    }

    /**
     * Cancel was clicked.
     */
    public void cancel() {
        finish();
    }

	/**
	 * 
	 */
	public void login() {
		EditText textUsername = (EditText)findViewById(R.id.editTextUserName);
		EditText textPassword = (EditText)findViewById(R.id.editTextPassword);

		textUsername.setError(null);
		textPassword.setError(null);
		
		String username = textUsername.getText().toString();
		String password = textPassword.getText().toString();

		boolean validFields = true;
		if (TextUtils.isEmpty(username)) {
			textUsername.setError(getString(R.string.error_missing_field));
			validFields = false;
		} 

		if (TextUtils.isEmpty(password)){
			textPassword.setError(getString(R.string.error_missing_field));
			validFields= false;
		} 

		if (validFields) {
		    Profile profile = new Profile();
		    profile.setUsername(username.trim());
		    profile.setPassword(password.trim());

		    LoginCommand loginCommand = new LoginCommand(this, profile, new ErrorDialogCompleteListener(this) {
		        @Override
		        public void onSuccess() {
		            setResult(RESULT_OK);
		            super.onSuccess();
		        }
		    });
		    loginCommand.execute();
		}
	}

	/**
	 * 
	 * @param view
	 */
    public void onNotRegisteredClicked(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
