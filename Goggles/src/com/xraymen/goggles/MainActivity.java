package com.xraymen.goggles;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.xraymen.goggles.MessageRetrievalService.MessageRetrievalServiceBinder;
import com.xraymen.goggles.R;
import com.xraymen.goggles.actionbar.ActionBarActivity;
import com.xraymen.goggles.commands.LogoutCommand;

import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;

public class MainActivity extends ActionBarActivity {
    
    private static final int LOGIN_BEFORE_COMPOSE_REQUEST = 12;

    private FragmentTabHost _tabHost = null;
    
    private static final Logger LOGGER = Logger.getLogger(MainActivity.class.getName());
    
    private MessageRetrievalServiceBinder _messageRetrievalServiceBinder = null;
    
    private final ServiceConnection _messageRetrievalServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder) {
            _messageRetrievalServiceBinder = (MessageRetrievalServiceBinder) binder;
            _messageRetrievalServiceBinder.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            _messageRetrievalServiceBinder.pause();
            _messageRetrievalServiceBinder = null;
        }
    };
    
    private final BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(MainActivity.this, getString(R.string.error_network_error), Toast.LENGTH_LONG).show();
        }
    };

    /**
     * 
     */
    public MainActivity() {
        super(R.layout.titlebar_main, R.id.titlebar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkLocationEnabled();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        
        // Introduction dialog
        if (!GogglesApplication.getApplication().isIntroCompleted() && preferences.getBoolean(getString(R.string.prefs_showIntroductionKey), true)) {
            IntroductionActivity d = new IntroductionActivity();
            d.show(getSupportFragmentManager(), "dialog");
        }

        setContentView(R.layout.activity_main);
        
        Resources resources = getResources();
        
        _tabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
        _tabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        _tabHost.getTabWidget().setDividerDrawable(R.drawable.tab_divider);

        addTab(resources.getString(R.string.title_activity_list), _tabHost, MessageListActivity.class);

        addTab(resources.getString(R.string.title_activity_map), _tabHost, MapActivity.class);

        //addTab(resources.getString(R.string.title_activity_xray), _tabHost, XRayActivity.class);

        _tabHost.setCurrentTab(0);
        
        LocalBroadcastManager.getInstance(this).registerReceiver(_broadcastReceiver,
                new IntentFilter(Notifications.RETRIEVE_FAILED));
        
        bindService(new Intent(this, MessageRetrievalService.class), _messageRetrievalServiceConnection, BIND_AUTO_CREATE);
        startService(new Intent(this, LocationService.class));

        LOGGER.log(Level.INFO, "onCreate");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        if (_messageRetrievalServiceBinder != null) {
            unbindService(_messageRetrievalServiceConnection);
        }
        stopService(new Intent(this, LocationService.class));
        
        LocalBroadcastManager.getInstance(this).unregisterReceiver(_broadcastReceiver);

        LOGGER.log(Level.INFO, "onDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isLoggedIn = GogglesApplication.getApplication().isLoggedIn();
        menu.findItem(R.id.action_login).setVisible(!isLoggedIn);
        menu.findItem(R.id.action_logout).setVisible(isLoggedIn);
        menu.findItem(R.id.action_register).setVisible(!isLoggedIn);
        return true;
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        LOGGER.info("onStart");
        
        if (_messageRetrievalServiceBinder != null) {
            _messageRetrievalServiceBinder.start();
        }
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        LOGGER.info("onStop");
        
        if (_messageRetrievalServiceBinder != null) {
            _messageRetrievalServiceBinder.pause();
        }
    }

    /**
     * 
     * @param title
     * @param tabHost
     * @param contentFragment
     */
    private void addTab(String title, FragmentTabHost tabHost, Class<? extends Fragment> contentFragment) {
        View view = LayoutInflater.from(this).inflate(R.layout.tab, null);
        TabSpec tabSpec = tabHost.newTabSpec(title).setIndicator(view);
        ((TextView)view.findViewById(R.id.tabTitle)).setText(title.toUpperCase(Locale.US));
        tabHost.addTab(tabSpec, contentFragment, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    
        boolean handled = true;

        int id = item.getItemId();

        switch(id) {
        case(R.id.action_login):
            startActivity(new Intent(this, LoginActivity.class));
            break;
        case(R.id.action_logout): 
            LogoutCommand command = new LogoutCommand(this, null);
            command.execute();
            break;
        case (R.id.action_register):
            startActivity(new Intent(this, RegisterActivity.class));
            break;
        case R.id.action_compose:
            if (!GogglesApplication.getApplication().isLoggedIn()) {
                startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_BEFORE_COMPOSE_REQUEST);
            } else {
                startActivity(new Intent(this, NewMessageActivity.class));
            }
            break;
        case R.id.action_help:
            startActivity(new Intent(this, HelpActivity.class));
            break;
        case R.id.action_settings:
            startActivity(new Intent(this, SettingsActivity.class));
            break;
        default:
            handled = false;
            break;
        }
        return handled;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == LOGIN_BEFORE_COMPOSE_REQUEST) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, NewMessageActivity.class));
            }
        }  else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }
    
    /**
     * 
     */
    private void checkLocationEnabled() {
        
        if (GogglesApplication.getApplication().isLocationProvidersChecked()) {
            return;
        }
        
        LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            String errorMessage = getString(R.string.error_no_gps);
            if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                errorMessage = getString(R.string.error_no_location_provider);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(errorMessage);
            builder.setPositiveButton(R.string.yes, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //No providers. Start the settings Android location settings activity.
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    dialog.cancel();
                    GogglesApplication.getApplication().setLocationProvidersChecked(true);
                }
            });
            builder.setNegativeButton(R.string.no, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    GogglesApplication.getApplication().setLocationProvidersChecked(true);
                }
            });
            builder.create().show();
        }
    }
}
