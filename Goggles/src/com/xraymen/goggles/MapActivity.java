package com.xraymen.goggles;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.LocationSource.OnLocationChangedListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.xraymen.goggles.LocationService.LocationServiceBinder;
import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.db.Settings;
import com.xraymen.goggles.db.SettingsTableHelper;

public class MapActivity extends SupportMapFragment {

    private static final double ZOOM_LEVEL_0_METERS = 20088000.566077;
    private static final int CIRCLE_FILL_COLOR =    0x10FF0000;
    private static final int CIRCLE_STROKE_COLOR =  0x40FF0000;
    private static final float CIRCLE_STROKE_WIDTH = 3f;

    private BitmapDescriptor _markerIcon = null; 

    private Location _location = null;
    private LocationServiceBinder _locationServiceBinder;
    private Settings _settings = null;
    private OnLocationChangedListener _mapLocationListener = null;

    /** 
     * Map used to launch message details activity when a marker info window is clicked
     */
    private HashMap<Marker, Message> _markerMap = new HashMap<Marker, Message>();

    /**
     * 
     */
    private final BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Notifications.LOCATION_UPDATE.equals(intent.getAction())) {
                Location location = (Location)intent.getExtras().getParcelable(LocationService.EXTRA_LOCATION);
                onLocationChanged(location);
            } else if (Notifications.MESSAGES_UPDATED.equals(intent.getAction())) {
                updateMapObjects();
            }
        }
    };

    /**
     * 
     */
    private final ServiceConnection _locationServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            _locationServiceBinder = null;
        }
        
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _locationServiceBinder = (LocationServiceBinder)service;
            Location location = _locationServiceBinder.getCurrentLocation();
            if (location != null) {
                onLocationChanged(location);
            }
        }
    }; 

    /**
     * 
     */
    private final OnInfoWindowClickListener _onInfoWindowClickListener = new OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker) {
            Message message = _markerMap.get(marker);
            if (message != null) {
                Intent intent = new Intent(getActivity(), MessageDetailsActivity.class);
                intent.putExtra(MessageDetailsActivity.MESSAGE_ID_KEY, (long)message.getLocalId());
                startActivity(intent);
            }
        }
    };
    
    /**
     * Location source for the google map.
     */
    private final LocationSource _locationSource = new LocationSource() {

        @Override
        public void activate(OnLocationChangedListener listener) {
            _mapLocationListener = listener;
        }

        @Override
        public void deactivate() {
            _mapLocationListener = null;
        }
    };
    
    @Override
    public void onStart() {
        super.onStart();
        
        GoogleMap map = getMap();
        if (map != null) {
            map.setOnInfoWindowClickListener(_onInfoWindowClickListener);
            //Disable zoom controls.
            map.getUiSettings().setZoomControlsEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);
            map.setLocationSource(_locationSource);
            
            if (_markerIcon == null) {
                _markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.icon_map_marker);
            }
        }
        
        SettingsTableHelper tableHelper = new SettingsTableHelper();
        _settings = tableHelper.getSettings();
        
        //Bind to location service to get current location        
        getActivity().bindService(new Intent(getActivity(), LocationService.class), _locationServiceConnection, Context.BIND_AUTO_CREATE);

        //Register for location updates
        IntentFilter intentFilter = new IntentFilter(); 
        intentFilter.addAction(Notifications.LOCATION_UPDATE);
        intentFilter.addAction(Notifications.MESSAGES_UPDATED);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(_broadcastReceiver, intentFilter);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        
        GoogleMap map = getMap();
        if (map != null) {
            map.setOnInfoWindowClickListener(null); 
        }

        if (_locationServiceBinder != null) {
            getActivity().unbindService(_locationServiceConnection);
        }

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(_broadcastReceiver);
    }
    
    /**
     * Point map camera to current location
     */
    private void centerMap() {
        GoogleMap map = getMap();
        if (map != null && _location != null) {
            //Initial center;
            LatLng center = new LatLng(_location.getLatitude(), _location.getLongitude());
            CameraUpdate update = CameraUpdateFactory.newLatLng(center);
            map.moveCamera(update);
        }
    }
    
    /**
     * Zoom map 
     */
    private void zoomMap() {
        GoogleMap map = getMap();
        if (map != null) {
            //How to convert meters to a zoom level from here: 
            // http://stackoverflow.com/questions/7199790/jquery-function-to-return-google-map-zoom-level
            double zoomFactor = Math.log(ZOOM_LEVEL_0_METERS / _settings.getRadiusHorizontal()) / Math.log(2); 
            map.moveCamera(CameraUpdateFactory.zoomTo((float)zoomFactor));
        }
    }

    /**
     * 
     */
    private void updateMapObjects() {
        GoogleMap map = getMap();
        if (map != null) {
            map.clear();
            updateMapLocation(map);
            updateMapMarkers(map);
        }
    }

    /**
     * Show 
     */
    private void updateMapLocation(GoogleMap map) {
        if (_location != null) {
            LatLng center = new LatLng(_location.getLatitude(), _location.getLongitude());
            final double radius = _settings.getRadiusHorizontal();
            map.addCircle(new CircleOptions().radius(radius).center(center).fillColor(CIRCLE_FILL_COLOR).strokeColor(CIRCLE_STROKE_COLOR)
                    .strokeWidth(CIRCLE_STROKE_WIDTH));
        }
    }

    /**
     * 
     */
    private void updateMapMarkers(GoogleMap map) {

        _markerMap.clear();

        MessageTableHelper messageTable = new MessageTableHelper();
        ArrayList<Message> messages = messageTable.getMessages();
        for (Message message : messages) {
            Marker marker = map.addMarker(createMarker(message));
            _markerMap.put(marker, message);
        }
    }

    /**
     * 
     * @param message
     * @return
     */
    private MarkerOptions createMarker(Message message) {

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.draggable(false);
        markerOptions.position(new LatLng(message.getLatitude(), message.getLongitude()));
        DateFormat timeFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        markerOptions.snippet(String.format("%s - %s", message.getOwnerId(),timeFormat.format(message.getCreated())));
        markerOptions.title(message.getText());
        markerOptions.icon(_markerIcon);
        return markerOptions;
    }

    /**
     * @param location
     */
    private void onLocationChanged(Location location) {
        boolean firstLocationUpdate = _location == null;
        _location = location;
        if (firstLocationUpdate) {
            //Only zoom on the first location update
            zoomMap();
        }
        centerMap();
        updateMapObjects();
        
        //Update the map's location indicator.
        if (_mapLocationListener != null) {
            _mapLocationListener.onLocationChanged(location);
        }
    }
}
