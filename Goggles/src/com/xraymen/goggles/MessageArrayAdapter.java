package com.xraymen.goggles;

import java.text.DateFormat;
import java.util.ArrayList;

import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MessageArrayAdapter extends ArrayAdapter<Message> {

    private static final int BGCLR_UNKNOWN = 0xFFFFFF80;
    private static final int BGCLR_ERROR = 0xFFFF8080;
    private static final int BGCLR_POSTED = 0;
    
    LayoutInflater _layoutInflater;
    
    public MessageArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);

        _layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        update();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        
        if (view == null) {
            view = _layoutInflater.inflate(R.layout.messagelistentry, null);
        }
        
        Message message = getItem(position);
        
        TextView contentView = (TextView)view.findViewById(R.id.messageContent);
        TextView authorView = (TextView)view.findViewById(R.id.messageAuthor);
        TextView dateView = (TextView)view.findViewById(R.id.messageDate);
        
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);

        contentView.setText(message.getText());
        authorView.setText(message.getOwnerId());
        dateView.setText(dateFormat.format(message.getCreated()));

        if (message.getStatus() == Message.STATUS_POSTED) {
            view.setBackgroundColor(BGCLR_POSTED);
        } else if (message.getStatus() == Message.STATUS_ERROR) {
            view.setBackgroundColor(BGCLR_ERROR);
        } else {
            view.setBackgroundColor(BGCLR_UNKNOWN);
        }

        //Update the ratings icons
        View ratingGroup = view.findViewById(R.id.ratingGroup);
        if (!message.isSupportsFeedback() || (message.getRatingLikes() == 0 && message.getRatingDislikes() == 0)) {
            ratingGroup.setVisibility(View.GONE);
        } else {
            ratingGroup.setVisibility(View.VISIBLE);
            
            TextView likeCount = (TextView) view.findViewById(R.id.ratingLike);
            TextView dislikeCount = (TextView) view.findViewById(R.id.ratingDislike);

            likeCount.setText(getRatingCountString(message.getRatingLikes()));
            dislikeCount.setText(getRatingCountString(message.getRatingDislikes()));

            int likeDrawableId = R.drawable.icon_rating_like_disabled;
            int dislikeDrawableId = R.drawable.icon_rating_dislike_disabled;
            
            if (GogglesApplication.getApplication().isLoggedIn()) {
                likeDrawableId = message.getRatingUser() > 0 ? R.drawable.icon_rating_like_selected : R.drawable.icon_rating_like_enabled;
                dislikeDrawableId = message.getRatingUser() < 0 ? R.drawable.icon_rating_dislike_selected : R.drawable.icon_rating_dislike_enabled;
            }
            likeCount.setCompoundDrawablesWithIntrinsicBounds(likeDrawableId, 0, 0, 0);
            dislikeCount.setCompoundDrawablesWithIntrinsicBounds(dislikeDrawableId, 0, 0, 0);
        }
        return view;
    }
    
    @Override
    public long getItemId(int position) {
        return getItem(position).getLocalId();
    }
    
    /**
     * 
     */
    public void update() {
        
        int sortOrder = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getContext().getString(R.string.prefs_list_sort_key), Integer.toString(MessageTableHelper.SORT_CREATED)));
        
        clear();
        MessageTableHelper tableHelper = new MessageTableHelper();
        ArrayList<Message> messages = tableHelper.getMessages(sortOrder);
        for (Message message : messages) {
            add(message);
        }
    }
    
    /**
     * 
     * @param rating
     * @return
     */
    @SuppressLint("DefaultLocale")
    private static String getRatingCountString(int rating) {
        if (rating < 1000) {
            return Integer.toString(rating);
        } else if (rating < 1000000) {
            return String.format("%.1fk", rating / 1000f);
        }
        return String.format("%.1fM", rating / 1000000f);
    }
}
