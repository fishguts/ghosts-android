package com.xraymen.goggles;

import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Logger;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.xraymen.goggles.actionbar.ActionBarActivity;
import com.xraymen.goggles.commands.AsyncCommandBase.CompleteListener;
import com.xraymen.goggles.commands.DeleteMessageCommand;
import com.xraymen.goggles.commands.FlagMessageCommand;
import com.xraymen.goggles.commands.HttpCommandBase;
import com.xraymen.goggles.commands.RatingsCommand;
import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.db.Profile;
import com.xraymen.goggles.db.ProfileTableHelper;

import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MessageDetailsActivity extends ActionBarActivity {

    private static final Logger LOGGER = Logger.getLogger(MessageDetailsActivity.class.getName());
    
    private static final int MAP_ZOOM_LEVEL = 18;

    public final static String MESSAGE_ID_KEY = "messageId";

    private Message _message;
    
    private static final int LOGIN_BEFORE_COMMAND_REQUEST = 13;
    
    private HttpCommandBase _command = null;
    
    private boolean _isRunning = false;

    public MessageDetailsActivity() {
        super(R.layout.titlebar_empty, R.id.titlebar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_details);
        
        LOGGER.info("onCreate");

        long _messageId = getIntent().getLongExtra(MESSAGE_ID_KEY, 0);
        
        //Get the message.
        MessageTableHelper messageTableHelper = new MessageTableHelper();
        _message = messageTableHelper.getMessage(_messageId);
        if (_message == null) {
            finish();
            return;
        }
        
        populateTextBoxes();
        
        ((TextView)findViewById(R.id.textViewText)).setMovementMethod(new ScrollingMovementMethod());
        
        updateButtons();

        //Show a map showing the location of the message.
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.map);
        if (fragment != null) {
            SupportMapFragment mapFragment = (SupportMapFragment)fragment;
            GoogleMap map = mapFragment.getMap();
            if (map != null) {
                map.getUiSettings().setZoomControlsEnabled(false);
                LatLng latLng = new LatLng(_message.getLatitude(), _message.getLongitude());
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM_LEVEL));
                map.addMarker(new MarkerOptions().draggable(false).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_map_marker)));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.empty, menu);
        return true;
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        _isRunning = true;
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        _isRunning = false;
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        LOGGER.info("onDestroy");
    }
    
    /**
     * 
     * @param v
     */
    public void onLikeClicked(View v) {
        v.setEnabled(false);

        RatingsCommand command = new RatingsCommand(this, _message, 1, new UpdateButtonsCompleteListener());
        executeCommand(command);
    }

    /**
     * 
     * @param v
     */
    public void onDislikeClicked(View v) {
        v.setEnabled(false);

        RatingsCommand command = new RatingsCommand(this, _message, -1, new UpdateButtonsCompleteListener());
        executeCommand(command);
    }
    
    /**
     * 
     * @param v
     */
    public void onFlagClicked(View v) {
        FlagMessageCommand command = new FlagMessageCommand(this, _message, new UpdateButtonsCompleteListener());
        executeCommand(command);
    }
    
    /**
     * 
     * @param v
     */
    public void onDeleteClicked(View v) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setPositiveButton(R.string.yes, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeleteMessageCommand command = new DeleteMessageCommand(MessageDetailsActivity.this, _message, new ErrorDialogCompleteListener(MessageDetailsActivity.this));
                executeCommand(command);                
            }
        });

        dialogBuilder.setNegativeButton(R.string.no, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogBuilder.setMessage(R.string.delete_alert);

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }
    
    /**
     * 
     * @param command
     */
    private void executeCommand(HttpCommandBase command) {
        if (!GogglesApplication.getApplication().isLoggedIn()) {
            _command = command;
            startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_BEFORE_COMMAND_REQUEST);
        } else {
            command.execute();
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == LOGIN_BEFORE_COMMAND_REQUEST) {
            if (resultCode == RESULT_OK) {
                _command.execute();
            } else {
                updateButtons();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    /**
     * 
     */
    private void populateTextBoxes() {
        populateTextViewString(R.id.textViewAuthor, _message.getOwnerId());
        populateTextViewString(R.id.textViewText, _message.getText());
        populateTextViewDate(R.id.textViewCreatedDate,  _message.getCreated());
        populateTextViewDate(R.id.textViewExpirationDate, _message.getExpiration());

        TextView errorBox = (TextView)findViewById(R.id.textViewError);
        if (_message.getStatus() == Message.STATUS_ERROR) {
            errorBox.setText(_message.getError());
        } else {
            errorBox.setVisibility(View.GONE);
        }
    }
    
    /**
     * 
     * @param textViewId
     * @param string
     */
    private void populateTextViewString(int textViewId, String string) {
        if (!TextUtils.isEmpty(string)) {
            TextView view = (TextView)findViewById(textViewId);
            view.setText(string);
        }
    }
    
    /**
     * 
     * @param textViewDateId
     * @param date
     */
    private void populateTextViewDate(int textViewDateId, Date date) {

        TextView dateView = (TextView)findViewById(textViewDateId);

        if (date != null) {
            DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
            dateView.setText(dateFormat.format(date));
        } else {
            dateView.setText(R.string.never);
        }
    }
    
    /**
     * 
     */
    private void updateButtons() {
        Button likeButton = (Button)findViewById(R.id.likeButton);
        Button dislikeButton = (Button)findViewById(R.id.dislikeButton);
        View flagButton = findViewById(R.id.flagButton);
        View deleteButton = findViewById(R.id.deleteButton);

        //Get login profile to determine if current user is owner of this message.
        ProfileTableHelper profileTable = new ProfileTableHelper();
        Profile loginProfile = profileTable.getLoginProfile();
        boolean userOwnsMessage = (GogglesApplication.getApplication().isLoggedIn() && loginProfile != null && loginProfile.getUsername()
                .equalsIgnoreCase(_message.getOwnerId()));

        if (!userOwnsMessage) {
            deleteButton.setVisibility(View.GONE);
        }
        
        if (_message.isSupportsFeedback()) {
            if (userOwnsMessage) {
                //Hide flag button when owner of message
                flagButton.setVisibility(View.GONE);
            } else {
                flagButton.setEnabled(!_message.getFlaggedUser());
            }
            
            if (GogglesApplication.getApplication().isLoggedIn()) {
                likeButton.setEnabled(_message.getRatingUser() <= 0);
                dislikeButton.setEnabled(_message.getRatingUser() >= 0);
            }

            if (_message.getRatingLikes() > 0) {
                likeButton.setText(String.format("%s (%d)", getString(R.string.like), _message.getRatingLikes()));                
            } else {
                likeButton.setText(getString(R.string.like));
            }
            if (_message.getRatingDislikes() > 0) {
                dislikeButton.setText(String.format("%s (%d)", getString(R.string.dislike), _message.getRatingDislikes()));                
            } else {
                dislikeButton.setText(getString(R.string.dislike));
            }
            
        } else {
            flagButton.setVisibility(View.GONE);
            likeButton.setVisibility(View.GONE);
            dislikeButton.setVisibility(View.GONE);
        }
    }
    
    /**
     * 
     * @return
     */
    private boolean isRunning() {
        return _isRunning;
    }
    
    /**
     * 
     * @author amitlissack
     *
     */
    private class UpdateButtonsCompleteListener implements CompleteListener {

        public UpdateButtonsCompleteListener() {
        }

        @Override
        public void onSuccess() {
            if (isRunning()) {
                runOnUiThread(new Runnable() {
    				@Override
    				public void run() {
    					updateButtons();
    				}
            	});
            }
        }

        @Override
        public void onFailure(final String description) {
            if (isRunning()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        updateButtons();

                        AlertDialog d = new AlertDialog.Builder(MessageDetailsActivity.this).create();
                        d.setMessage(description);
                        d.setTitle(R.string.app_name);
                        d.show();
                    }
                });
            }
        }
    }
}
