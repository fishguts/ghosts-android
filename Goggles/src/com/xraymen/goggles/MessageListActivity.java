package com.xraymen.goggles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MessageListActivity extends Fragment implements OnItemClickListener {

    private final BroadcastReceiver _messagesUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshList();            
        }
    };
    
    private MessageArrayAdapter _cursorAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_message_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        _cursorAdapter = new MessageArrayAdapter(getActivity(), 0);
        ListView lv = (ListView)getActivity().findViewById(R.id.messageListView);
        lv.setOnItemClickListener(this);
        lv.setAdapter(_cursorAdapter);
    }
    
    @Override
    public void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Notifications.MESSAGES_UPDATED);
        intentFilter.addAction(Notifications.LOGIN_CHANGED);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(_messagesUpdatedReceiver,
                intentFilter);

        refreshList();
    }
    
    @Override
    public void onStop() {
        super.onStop();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(_messagesUpdatedReceiver);
    }
    
    /**
     * 
     */
    private void refreshList() {
        _cursorAdapter.update();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), MessageDetailsActivity.class);
        intent.putExtra(MessageDetailsActivity.MESSAGE_ID_KEY, id);
        startActivity(intent);
    }
}
