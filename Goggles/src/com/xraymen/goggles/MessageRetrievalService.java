package com.xraymen.goggles;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.xraymen.goggles.PausableTimer.OnFireListener;
import com.xraymen.goggles.commands.AsyncCommandBase.CompleteListener;
import com.xraymen.goggles.commands.LoginCommand;
import com.xraymen.goggles.commands.MessagesGetCommand;
import com.xraymen.goggles.commands.PostMessageCommand;
import com.xraymen.goggles.commands.SettingsGetCommand;
import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.db.Settings;
import com.xraymen.goggles.db.SettingsTableHelper;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

public class MessageRetrievalService extends Service implements OnFireListener  {
    
    public static final Logger LOGGER = Logger.getLogger(MessageRetrievalService.class.getName());

    private boolean _running = false;
    
    private Location _currentLocation = null;
    
    private Date _lastRequestTime = null;
    
    private PausableTimer _pollTimer = null;
    
    private PausableTimer _welcomeStateTimer = null;
    
    private boolean _welcomeState = false;
    
    private Settings _settings; 
    
    private static final int WELCOME_STATE_TIME = 60 * 2 * 1000;
    
    private final MessageRetrievalServiceBinder _binder = new MessageRetrievalServiceBinder();
    
    private LocationService.LocationServiceBinder _locationServiceBinder = null;
    
    /**
     * Location service connection.
     */
    private final ServiceConnection _locationServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _locationServiceBinder = (LocationService.LocationServiceBinder) service;
            _currentLocation = _locationServiceBinder.getCurrentLocation();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            _locationServiceBinder = null;
        }
    };

    /**
     * Receiver of notifications
     */
    private final BroadcastReceiver _broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Notifications.LOCATION_UPDATE.equals(intent.getAction())) {
                Location location = (Location)intent.getExtras().getParcelable(LocationService.EXTRA_LOCATION);
                onLocationUpdated(location);
            } else if (Notifications.LOGIN_CHANGED.equals(intent.getAction())) {
                LOGGER.info("Login Changed");
                //Login has changed. 
                if (_running) {
                    //We're running. Request messages.
                    requestMessages();
                    retryFailedMessages();
                } else {
                    //Force request when we next startup.
                    _lastRequestTime = null;
                }
            }
        }
    };

    @Override
    public void onCreate() {
        LOGGER.info("onCreate");
        
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        
        IntentFilter filter = new IntentFilter();
        filter.addAction(Notifications.LOCATION_UPDATE);
        filter.addAction(Notifications.LOGIN_CHANGED);
        LocalBroadcastManager.getInstance(MessageRetrievalService.this).registerReceiver(_broadcastReceiver, filter);

        checkWelcomeState(preferences);
        
        makeInitialServerRequests(preferences);
    }
    
    /**
     * 
     */
    private void onStart() {

        if (_running) {
            return;
        }
        
        if (_settings == null) {
            SettingsTableHelper s = new SettingsTableHelper();
            _settings = s.getSettings();
        }
        
        _running = true;
        
        bindService(new Intent(this, LocationService.class), _locationServiceConnection, Context.BIND_AUTO_CREATE);

        if (_lastRequestTime == null) {
            requestMessages();
        } else if (_pollTimer != null) {
            _pollTimer.resume();
        }
        
        retryFailedMessages();
        
        LOGGER.info("onStart");
    }
    
    /**
     * 
     */
    private void onStop() {
        
        if (!_running) {
            return;
        } 
        
        _running = false;
        
        if (_locationServiceBinder != null) {
            unbindService(_locationServiceConnection);
        }

        if (_pollTimer != null) {
            _pollTimer.pause();
        }
        
        LOGGER.info("onStop");
    }

    @Override
    public void onDestroy() {
        LOGGER.info("onDestroy");
        
        if (_welcomeStateTimer != null) {
            _welcomeStateTimer.stop();
            _welcomeStateTimer = null;
        }
        
        if (_pollTimer != null) {
            _pollTimer.stop();
            _pollTimer = null;
        }
        
        LocalBroadcastManager.getInstance(this).unregisterReceiver(_broadcastReceiver);
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        return _binder;
    }
    
    /**
     * 
     * @param location
     */
    protected void onLocationUpdated(Location location) {
        LOGGER.info("onLocationUpdated last request was at " + (_lastRequestTime == null ? "NEVER" : _lastRequestTime.toString()));

        _currentLocation = location;
        
        if (!_running) {
            return;
        }

        final long queryIntervalMinSeconds = _settings.getQueryIntervalMin() * 1000;
        final long timeSinceLastRequest = _lastRequestTime == null ? Long.MAX_VALUE : (new Date().getTime() - _lastRequestTime.getTime());
        final long timeToWaitUntilNextRequest = queryIntervalMinSeconds - timeSinceLastRequest;

        if (timeSinceLastRequest >= queryIntervalMinSeconds) {
            requestMessages();
        } else if (_pollTimer != null) {
            //See how much is remaining on the timer. 
            final long remaining = _pollTimer.getRemaining();
            if (remaining <= 0 || remaining > timeToWaitUntilNextRequest) {
                //Only schedule next request if the time remaining on the timer is greater than how much we think we should wait
                scheduleNextRequest(timeToWaitUntilNextRequest);
            }
        } else {
            scheduleNextRequest(timeToWaitUntilNextRequest);
        }
    }

    /**
     * @param interval
     */
    private void scheduleNextRequest(long interval) {

        if (!_running) {
            return;
        }

        LOGGER.info(String.format("Scheduling next request for %d from now", interval));
        
        if (_pollTimer != null) {
            _pollTimer.stop();
        }
        _pollTimer = new PausableTimer(interval, this);
        _pollTimer.start();
    }

    /**
     * 
     */
    private void requestMessages() {
        if (_running && _currentLocation != null) {
            MessagesGetCommand command = new MessagesGetCommand(this, _currentLocation, _welcomeState, null );
            command.execute();
            _lastRequestTime = new Date();
            
            scheduleNextRequest(_settings.getQueryIntervalMax() * 1000);
        }
    }
    
    /**
     * 
     */
    private void retryFailedMessages() {
        if (_running && GogglesApplication.getApplication().isLoggedIn()) {
            MessageTableHelper tableHelper = new MessageTableHelper();
            ArrayList<Message> messages = tableHelper.getFailedMessages();
            for (Message message : messages) {
                LOGGER.info("Retrying message with id " + message.getLocalId());
                PostMessageCommand command = new PostMessageCommand(this, message, null);
                command.execute();
            }
        }
    }
    
    /**
     * @param preferences
     */
    private void makeInitialServerRequests(SharedPreferences preferences) {
        
        //Get the settings
        SettingsGetCommand command = new SettingsGetCommand(this, new CompleteListener() {

            @Override
            public void onSuccess() {
                SettingsTableHelper settingsTable = new SettingsTableHelper();
                _settings = settingsTable.getSettings(); 
            }
            
            @Override
            public void onFailure(String description) {
                LOGGER.log(Level.SEVERE, "Get settings failed:" + description);
            }
        });
        command.execute();
        
        //Auto log in
        if (!GogglesApplication.getApplication().isLoggedIn()) {
            if (preferences.getBoolean(PreferencesUtils.KEY_AUTO_LOGIN, false)) {
                LoginCommand login = new LoginCommand(this, null);
                login.execute();
            }
        }
    }

    /**
     * @param preferences
     */
    private void checkWelcomeState(SharedPreferences preferences) {
        _welcomeState = preferences.getBoolean(PreferencesUtils.KEY_WELCOME_STATE, true);  
        if (_welcomeState) {
            // Set a timer to take us out of welcome state.
            _welcomeStateTimer = new PausableTimer(WELCOME_STATE_TIME, this);
            _welcomeStateTimer.start();
        }
    }
    
    @Override
    synchronized public void onFire(PausableTimer t) {
        if (t.equals(_pollTimer)) {
            requestMessages();
            retryFailedMessages();
        } else if (t.equals(_welcomeStateTimer)) {
            LOGGER.info("Disabling welcome state");

            _welcomeState = false;
            Editor editor = PreferenceManager.getDefaultSharedPreferences(MessageRetrievalService.this).edit();
            editor.putBoolean(PreferencesUtils.KEY_WELCOME_STATE, false);
            editor.commit();
        }
    }
    
    /**
     * Binder used to control state.
     */
    public class MessageRetrievalServiceBinder extends Binder {

        public void start() {
            onStart();
        }
        
        public void pause() {
            onStop();
        }
    }
}
