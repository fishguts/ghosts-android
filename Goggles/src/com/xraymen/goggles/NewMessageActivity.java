package com.xraymen.goggles;

import java.text.DateFormat;
import java.util.Date;

import com.xraymen.goggles.actionbar.ActionBarActivity;
import com.xraymen.goggles.commands.PostMessageCommand;
import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.db.ProfileTableHelper;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class NewMessageActivity extends ActionBarActivity {

    //TODO: Here for now. Should be somewhere else.
    static final long HOUR = 60 * 60 * 1000;
    static final long DAY_MINUTES = 24 * HOUR; 
    static final long[] EXPIRATIONS = new long[] { 
            0, // No expiration
            HOUR, // 1 hour
            DAY_MINUTES,// 1 day
            DAY_MINUTES * 7,// 1 week
            DAY_MINUTES * 31,// 1 month
            DAY_MINUTES * 365,// 1 year
    };
    static final int TOP_OF_HOUR_INDEX = 1;

    private static final int MESSAGE_LENGTH_MIN = 2;
    private static final int MESSAGE_LENGTH_MAX = 512;
    
    private LocationService.LocationServiceBinder _locationServiceBinder = null;
    private ServiceConnection _locationServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _locationServiceBinder = (LocationService.LocationServiceBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            _locationServiceBinder = null;
        }
    };

    /**
     * 
     */
    public NewMessageActivity() {
        super(R.layout.titlebar_post, R.id.titlebar);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_message);
        
        // Was hard for me to figure out how to hide the virtual keyboard when
        // expiration spinner is clicked. Tried onFocusChanged, onItemClick,
        // onClick, and nothing worked. So instead I use touch release on the
        // spinner. Works.
        ((Spinner)findViewById(R.id.spinnerExpiration)).setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(R.id.editTextMessage).getWindowToken(), 0);
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post, menu);
        return true;
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
        bindService(new Intent(this, LocationService.class), _locationServiceConnection, Context.BIND_AUTO_CREATE);
        
        Spinner expirationControl = (Spinner)findViewById(R.id.spinnerExpiration);
        
        //Get the resource array of expiration names
        String [] expirationValues = getResources().getStringArray(R.array.expiration_values);
        
        //Overwrite the hour index with top of next hour.
        expirationValues[TOP_OF_HOUR_INDEX] = DateFormat.getTimeInstance(DateFormat.SHORT).format(getTopOfNextHour());
        
        //Create adapter and update expiration spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, expirationValues);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        expirationControl.setAdapter(adapter);
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        
        if (_locationServiceBinder != null) {
            unbindService(_locationServiceConnection);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;
            
        int id = item.getItemId();
        
        switch (id) {
        case R.id.action_home:
            cancel();
            break;
        case R.id.action_post:
            post();
            break;
        default:
            handled = false;
            break;
        }
        return handled;
    }
    
    /**
     * Cancel was pressed.
     */
    public void cancel() {
        finish();
    }

    /**
     * Post the new message to the server.
     */
    public void post() {
        
        Location location = null;

        if (_locationServiceBinder == null || ((location = _locationServiceBinder.getCurrentLocation()) == null)) {
            AlertDialog d = new AlertDialog.Builder(this).create();
            d.setMessage(getResources().getText(R.string.error_no_location));
            d.setTitle(R.string.app_name);
            d.show();
            return;
        }

        EditText messageControl = (EditText)findViewById(R.id.editTextMessage);
        messageControl.setError(null);
        
        String messageText = messageControl.getText().toString().trim();
        if (TextUtils.isEmpty(messageText) || messageText.length() < MESSAGE_LENGTH_MIN || messageText.length() > MESSAGE_LENGTH_MAX) {
            messageControl.setError(String.format(getString(R.string.error_message_length), MESSAGE_LENGTH_MIN, MESSAGE_LENGTH_MAX));
            return;
        }

        Spinner expirationControl = (Spinner)findViewById(R.id.spinnerExpiration);

        int expirationIndex = expirationControl.getSelectedItemPosition();

        ProfileTableHelper p = new ProfileTableHelper();

        Message message = new Message();
        message.setLatitude(location.getLatitude());
        message.setLongitude(location.getLongitude());
        message.setAltitude(location.getAltitude());
        message.setText(messageText);
        Date createdDate = new Date();
        message.setCreated(createdDate);
        if (EXPIRATIONS[expirationIndex] != 0) {
            if (expirationIndex == TOP_OF_HOUR_INDEX) {
                message.setExpiration(getTopOfNextHour());
            } else {
                //Create expiration time which is created time plus expiration time. 
                message.setExpiration(new Date(createdDate.getTime() + (EXPIRATIONS[expirationIndex])));
            }
        }
        message.setOwnerId(p.getLoginProfile().getUsername());

        MessageTableHelper messageTableHelper = new MessageTableHelper();
        messageTableHelper.saveMessage(message);

        PostMessageCommand command = new PostMessageCommand(this, message, null);
        command.execute();

        finish();
    }
    
    /**
     * Get Date object at the top of next hour
     * @return Date at the top of next hour
     */
    static private Date getTopOfNextHour() {
        long now = new Date().getTime();
        Date date = new Date(now + (HOUR - (now % HOUR)));
        return date;
    }
}
