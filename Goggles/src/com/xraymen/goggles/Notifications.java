package com.xraymen.goggles;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class Notifications {

    public static final String LOCATION_UPDATE = "NewLocation";
    public static final String MESSAGES_UPDATED = "NewMessages";
    public static final String RETRIEVE_FAILED = "RetrieveFailed";
    public static final String LOGIN_CHANGED = "LoginChanged";
    
    /**
     * 
     * @param intent
     * @param context
     */
    public static void send(final Intent intent, final Context context) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
    
    /**
     * 
     * @param intent
     */
    public static void send(final Intent intent) {
        send(intent, GogglesApplication.getContext());
    }
    
    /**
     * 
     * @param notification
     */
    public static void send(final String notification) {
        send(new Intent(notification));
    }
}
