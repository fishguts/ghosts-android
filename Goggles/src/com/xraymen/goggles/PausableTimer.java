package com.xraymen.goggles;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class PausableTimer {

    private final static Logger LOGGER = Logger.getLogger(PausableTimer.class.getName());

    private Timer _timer = null;

    private OnFireListener _listener = null;

    private final long _interval;

    private long _remaining;

    private Date _startTime;

    /**
     * Constructor
     * 
     * @param interval
     * @param listener
     */
    public PausableTimer(long interval, OnFireListener listener) {
        _interval = interval;
        _remaining = _interval;
        _listener = listener;
    }

    /**
     * Start the timer
     */
    public void start() {
        stop();
        startTimer(_interval);
    }

    /**
     * Stop the timer
     */
    public void stop() {
        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }
    }

    /**
     * Pause the timer
     */
    public void pause() {
        if (_timer != null) {
            _timer.cancel();
            _timer = null;
            _remaining = getRemaining();
            LOGGER.info(String.format("Pausing timer with %d to go", _remaining));
        }
    }

    /**
     * Resume the timer
     */
    public void resume() {
        if (_timer == null && _remaining > 0) {
            startTimer(_remaining);
        }
    }

    /**
     * Get time remaining
     * 
     * @return time remaining
     */
    public long getRemaining() {
        return _remaining - (new Date().getTime() - _startTime.getTime());
    }

    /**
     * @param interval
     */
    private void startTimer(long interval) {
        
        LOGGER.info(String.format("Starting timer with %d to go", interval));
        
        _startTime = new Date();
        _remaining = interval;
        _timer = new Timer();
        _timer.schedule(new TimerTask() {
            @Override
            public void run() {
                _listener.onFire(PausableTimer.this);
            }
        }, interval);
    }

    /**
     * 
     * @author amitlissack
     * 
     */
    public interface OnFireListener {
        public void onFire(PausableTimer t);
    }

}
