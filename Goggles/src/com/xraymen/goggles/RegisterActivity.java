package com.xraymen.goggles;

import com.xraymen.goggles.actionbar.ActionBarActivity;
import com.xraymen.goggles.commands.RegisterCommand;
import com.xraymen.goggles.db.Profile;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class RegisterActivity extends ActionBarActivity {

    private static final int MIN_USERNAME_LENGTH = 4;
    private static final int MIN_PASSWORD_LENGTH = 4;

    public RegisterActivity() {
        super(R.layout.titlebar_register, R.id.titlebar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_register);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.register, menu);
        return true;
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;
        switch(item.getItemId()) {
        case R.id.action_home:
            cancel();
            break;
        case R.id.action_register:
            post();
            break;
        default:
            handled = false;
            break;
        }
        return handled;
    }

    /**
     * Cancel was clicked
     */
    public void cancel() {
        finish();
    }

    /**
     * 
     */
    public void post() {
        EditText textUsername = (EditText) findViewById(R.id.editTextUserName);
        EditText textEmail = (EditText) findViewById(R.id.editTextEmail);
        EditText textPassword = (EditText) findViewById(R.id.editTextPassword);

        textUsername.setError(null);
        textEmail.setError(null);
        textPassword.setError(null);

        String username = textUsername.getText().toString();
        String email = textEmail.getText().toString();
        String password = textPassword.getText().toString();

        boolean validFields = true;
        if (TextUtils.isEmpty(username)) {
            textUsername.setError(getString(R.string.error_missing_field));
            validFields = false;
        } else if (username.length() < MIN_USERNAME_LENGTH) {
            textUsername.setError(String.format(getString(R.string.error_too_short), MIN_USERNAME_LENGTH));
            validFields = false;           
        }

        if (TextUtils.isEmpty(email)) {
            textEmail.setError(getString(R.string.error_missing_field));
            validFields = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            textEmail.setError(getString(R.string.error_not_email_address));
            validFields = false;
        }

        if (TextUtils.isEmpty(password)) {
            textPassword.setError(getString(R.string.error_missing_field));
            validFields = false;
        } else if (password.length() < MIN_PASSWORD_LENGTH) {
            textPassword.setError(String.format(getString(R.string.error_too_short), MIN_PASSWORD_LENGTH));
            validFields = false;           
        }

        if (validFields) {
            Profile profile = new Profile();
            profile.setUsername(username.trim());
            profile.setPassword(password.trim());
            profile.setEmail(email.trim());
            
            RegisterCommand command = new RegisterCommand(this, profile, new ErrorDialogCompleteListener(this));
            command.execute();
        }
    }
}
