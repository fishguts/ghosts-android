package com.xraymen.goggles;

import com.xraymen.goggles.actionbar.AbstractActionBarAdapter;
import com.xraymen.goggles.actionbar.IActionBarAdapter;
import com.xraymen.goggles.net.NetworkUtils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

public class SettingsActivity extends PreferenceActivity {
    
    protected static final int LOGIN_BEFORE_SENDFEEDBACK_REQUEST = 10;
    private IActionBarAdapter _actionBarAdapter;
    
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
                
        _actionBarAdapter = AbstractActionBarAdapter.createAdapter(this, R.layout.titlebar_empty, R.id.titlebar);
        _actionBarAdapter.onCreate(savedInstanceState);
        
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
        
        PreferenceScreen rootPrefScreen = getPreferenceScreen();
        
        PreferenceScreen privacyPolicy = (PreferenceScreen) rootPrefScreen.findPreference(getString(R.string.prefs_privacy_policy_key));
        privacyPolicy.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showLegal(NetworkUtils.getUrlPrivacyPolicy(), preference.getTitle().toString());
                return true;
            }
        });

        PreferenceScreen termsAndConditions = (PreferenceScreen)rootPrefScreen.findPreference(getString(R.string.prefs_terms_of_service_key));
        termsAndConditions.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showLegal(NetworkUtils.getUrlTermsAndConditions(), preference.getTitle().toString());
                return true;
            }
        });
        
        Preference serverVersion = rootPrefScreen.findPreference(getString(R.string.prefs_serverVersionKey));
        serverVersion.setSummary(PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.prefs_serverVersionKey), "unknown"));

        Preference clientVersion = rootPrefScreen.findPreference(getString(R.string.prefs_clientVersionKey));
        try {
            PackageInfo packageInfo;
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            clientVersion.setSummary(packageInfo.versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        
        Preference sendFeedback = rootPrefScreen.findPreference(getString(R.string.prefs_send_feedback_key));
        sendFeedback.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (!GogglesApplication.getApplication().isLoggedIn()) {
                    startActivityForResult(new Intent(SettingsActivity.this, LoginActivity.class), LOGIN_BEFORE_SENDFEEDBACK_REQUEST);
                } else {
                    startActivity(new Intent(SettingsActivity.this, FeedbackActivity.class));
                }
                return true;
            }
        });
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        
        _actionBarAdapter.onPostCreate(savedInstanceState);
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == LOGIN_BEFORE_SENDFEEDBACK_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                startActivity(new Intent(this, FeedbackActivity.class));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    /**
     * @param url
     * @param title
     */
    private void showLegal(String url, String title) {
        Intent i = new Intent(this, WebActivity.class);
        i.putExtra(WebActivity.EXTRA_TITLE, title);
        i.putExtra(WebActivity.EXTRA_URL, url);
        startActivity(i);
    }
    
}
