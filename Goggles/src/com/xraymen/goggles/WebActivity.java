package com.xraymen.goggles;

import com.xraymen.goggles.actionbar.ActionBarActivity;

import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.webkit.WebView;

public class WebActivity extends ActionBarActivity {
    
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_URL = "url";
    
    public WebActivity() {
        super(R.layout.titlebar_empty, R.id.titlebar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        
        Intent intent = getIntent();
        String url = intent.getStringExtra(EXTRA_URL);
        String title = intent.getStringExtra(EXTRA_TITLE);

        setTitle(title);

        WebView webView = (WebView)findViewById(R.id.webView);
        webView.loadUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.empty, menu);
        return true;
    }
}
