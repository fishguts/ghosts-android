package com.xraymen.goggles.actionbar;

import android.app.Activity;
import android.os.Build;

/**
 * Adapter for using action or title bars depending on version of android. 
 * Gingerbread and earlier will use a custom title bar and send menu click events to the activity for each button on the titlebar.
 * Post-Gingerbread will simply use an ActionBar using the menu layout.  
 * 
 * The titlebar layout buttons and option menu items must match ids.
 * 
 * Put menu layouts in res/menu-v11 with action bar items which should only be seen as titlebar buttons on Gingerbread and earlier.  
 * 
 * @author amitlissack
 * 
 */
public abstract class AbstractActionBarAdapter implements IActionBarAdapter {

    protected Activity _activity;
    protected int _titleBarLayoutId;
    protected int _titleBarViewId;

    /**
     * 
     * @param activity
     * @return
     */
    public static IActionBarAdapter createAdapter(Activity activity, int titleBarLayoutId, int titleBarViewId ) {
        IActionBarAdapter ret = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ret = new ActionBarAdapter(activity, titleBarLayoutId, titleBarViewId);
        } else {
            ret = new TitleBarAdapter(activity, titleBarLayoutId, titleBarViewId);
        }
        return ret;
    }

    /**
     * 
     * @param activity
     */
    protected AbstractActionBarAdapter(Activity activity, int titleBarLayoutId, int titleBarViewId) {
        _activity = activity;
        _titleBarLayoutId = titleBarLayoutId; 
        _titleBarViewId = titleBarViewId;
    }
}
