package com.xraymen.goggles.actionbar;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * An activity with an actionbar/titlebar depending on Android version.
 * 
 * @author amitlissack
 *
 */
public abstract class ActionBarActivity extends FragmentActivity {

    private IActionBarAdapter _titleBarAdapter;
    
    public ActionBarActivity(int titleBarLayoutId, int titelBarViewId) {
        _titleBarAdapter = AbstractActionBarAdapter.createAdapter(this, titleBarLayoutId, titelBarViewId);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _titleBarAdapter.onCreate(savedInstanceState);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        
        _titleBarAdapter.onPostCreate(savedInstanceState);
    }
}
