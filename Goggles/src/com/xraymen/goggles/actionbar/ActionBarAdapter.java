package com.xraymen.goggles.actionbar;

import android.app.Activity;
import android.os.Bundle;

/**
 * Title bar adapter for post-Gingerbread devices.
 * @author amitlissack
 *
 */
public class ActionBarAdapter extends AbstractActionBarAdapter {

    protected ActionBarAdapter(Activity activity, int titleBarLayoutId, int titleBarViewId) {
        super(activity, titleBarLayoutId, titleBarViewId);
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
    }

    @Override
    public void onPostCreate(Bundle saveInstanceState) {
    }
}
