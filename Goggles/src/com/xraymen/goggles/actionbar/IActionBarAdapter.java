package com.xraymen.goggles.actionbar;

import android.os.Bundle;

public interface IActionBarAdapter {

    public void onCreate(Bundle saveInstanceState);

    public void onPostCreate(Bundle saveInstanceState);
}
