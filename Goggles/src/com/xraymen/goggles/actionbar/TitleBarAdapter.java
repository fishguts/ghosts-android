package com.xraymen.goggles.actionbar;

import com.xraymen.goggles.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Adapter for Gingerbread devices. Basically, we convert all the buttons in the
 * custom title bar to menu click events for the activity as if they were coming
 * from an ActionBar
 * 
 * @author amitlissack
 * 
 */
public class TitleBarAdapter extends AbstractActionBarAdapter {

    /**
     * Constructor
     * @param activity
     * @param titleBarLayoutId
     * @param titleBarViewId
     */
    protected TitleBarAdapter(Activity activity, int titleBarLayoutId, int titleBarViewId) {
        super(activity, titleBarLayoutId, titleBarViewId);
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        _activity.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
    }

    @Override
    public void onPostCreate(Bundle saveInstanceState) {
        _activity.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, _titleBarLayoutId);

        //Get the title bar view group here.
        View titleBarView = _activity.findViewById(_titleBarViewId);
        if (titleBarView != null) {
            ViewGroup titleBarViewGroup = (ViewGroup) titleBarView;
            
            // Iterate through the children looking for buttons. Each button
            // will have an onClickListener attached which will convert button
            // clicks to menu events for the activity
            for (int i = 0; i < titleBarViewGroup.getChildCount(); i++) {
                View childView = titleBarViewGroup.getChildAt(i);

                if (childView instanceof Button) {
                    Button button = (Button) childView;
                    button.setOnClickListener(new OnClickListener() {
                        
                        @Override
                        public void onClick(View v) {
                            Button b = (Button) v;
                            MenuItem m = new FakeMenu(_activity, v.getId(), b.getText());
                            _activity.onMenuItemSelected(0, m);
                        }
                    });
                } else if (childView instanceof ImageButton) {
                    ImageButton button = (ImageButton) childView;
                    button.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            MenuItem m = new FakeMenu(_activity, v.getId(), "");
                            _activity.onMenuItemSelected(0, m);
                        }
                    });
                }
            }
        }

        View titleView = _activity.findViewById(R.id.titleTextView);
        if (titleView != null) {
            ((TextView)titleView).setText(_activity.getTitle());
        }
    }
}
