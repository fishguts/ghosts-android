package com.xraymen.goggles.commands;

public class AsyncCommandBase implements Command {

    private CompleteListener _listener;

    /**
     * 
     * @param listener
     */
    public AsyncCommandBase(CompleteListener listener) {
        _listener  = listener;
    }

    @Override
    public void execute() {
        fireFailure("Nothing to do!");
    }
    
    /**
     * 
     */
    protected void fireSuccess() {
        if (_listener != null) {
            _listener.onSuccess();
        }
    }
    
    /**
     * 
     * @param error
     */
    protected void fireFailure(final String error) {
        if (_listener != null) {
            _listener.onFailure(error);
        }
    }

    /**
     * 
     * @author Amit
     *
     */
    public interface CompleteListener {
        public void onSuccess();
        public void onFailure(final String description);
    }
}
