package com.xraymen.goggles.commands;

public interface Command {
    public void execute();
}
