package com.xraymen.goggles.commands;

import org.json.JSONObject;

import android.content.Context;

import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class DeleteMessageCommand extends HttpCommandBase {

    private Message _message;
    
    /**
     * 
     * @param context
     * @param message
     * @param listener
     */
    public DeleteMessageCommand(Context context, Message message, CompleteListener listener) {
        super(context, listener);
        _message = message;
    }
    
    @Override
    public void execute() {
        if (_message.getStatus() == Message.STATUS_POSTED) {
            super.execute();
        } else {
            MessageTableHelper tableHelper = new MessageTableHelper();
            tableHelper.removeMessage(_message);
            fireSuccess();
        }
    }
    
    @Override
    protected String getBody() {
        return NetworkUtils.buildDeleteMessageRequest(_message.getGlobalId());
    }
    
    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlDeleteMessage();
    }
    
    @Override
    public void onSuccess(final JSONObject response) {
        MessageTableHelper tableHelper = new MessageTableHelper();
        tableHelper.removeMessage(_message);
        super.onSuccess(response);
    }
}
