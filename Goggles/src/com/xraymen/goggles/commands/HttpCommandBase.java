package com.xraymen.goggles.commands;

import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

import com.xraymen.goggles.net.HttpJsonRequest;
import com.xraymen.goggles.net.HttpJsonRequest.JsonResponseListener;

public class HttpCommandBase extends AsyncCommandBase implements JsonResponseListener {

    protected Context _context;

    /**
     * Constructor
     * @param context
     * @param listener
     */
    public HttpCommandBase(Context context, CompleteListener listener) {
        super(listener);
        _context = context;
    }

    /**
     * Get url for this command
     * @return must not be null
     */
    protected String getUrl() {
        return null;
    };
    
    /**
     * Get body of command.
     * @return may be null
     */
    protected String getBody() {
        return null;
    }

    @Override
    public void execute() {
        String url = getUrl();
        String body = getBody();
        
        HttpJsonRequest request;
        if (TextUtils.isEmpty(url)) {
            throw new RuntimeException("No URL!!");
        }
        
        if (TextUtils.isEmpty(body)) {
            request = new HttpJsonRequest(_context, url, this);
        } else {
            request = new HttpJsonRequest(_context, url, body, this);
        }
        
        request.execute();
    }

    @Override
    public void onSuccess(final JSONObject response) {
        fireSuccess();
    }

    @Override
    public void onFailure(final String status, final String text) {
        fireFailure(text);
    }
}
