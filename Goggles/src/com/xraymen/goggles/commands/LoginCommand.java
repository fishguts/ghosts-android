package com.xraymen.goggles.commands;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.xraymen.goggles.GogglesApplication;
import com.xraymen.goggles.Notifications;
import com.xraymen.goggles.PreferencesUtils;
import com.xraymen.goggles.R;
import com.xraymen.goggles.db.Profile;
import com.xraymen.goggles.db.ProfileTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class LoginCommand extends HttpCommandBase {

    public static final Logger LOGGER = Logger.getLogger(LoginCommand.class.getName());
    
    private Profile _profile;

    /**
     * Login specific profile
     * @param context
     * @param profile
     * @param listener
     */
    public LoginCommand(Context context, final Profile profile, CompleteListener listener) {
        super(context, listener);
        _profile = profile;
    }

    /**
     * Login current login profile
     * @param context
     * @param listener
     */
    public LoginCommand(Context context, CompleteListener listener) {
        super(context, listener);
        ProfileTableHelper p = new ProfileTableHelper();
        _profile = p.getLoginProfile();
    }

    @Override
    public void execute() {
        if (_profile == null) {
            fireFailure(_context.getString(R.string.error_no_login_profile));
        } else {
            super.execute();
        }
    }
    
    @Override
    public void onSuccess(final JSONObject response) {
        ProfileTableHelper p = new ProfileTableHelper();

        Profile currentLoggedIn = p.getLoginProfile();
        if (currentLoggedIn != null && !currentLoggedIn.getUsername().equalsIgnoreCase(_profile.getUsername())) {
            //Logging in using a different user. Modify the last login user. 
            currentLoggedIn.setLoggedIn(false);
            currentLoggedIn.setLogin(false);
            p.saveProfile(currentLoggedIn);
        }

        try {

            JSONObject profileJSON;
            profileJSON = response.getJSONObject(NetworkUtils.KEY_PROFILE);
            _profile.setCreated(NetworkUtils.DATE_FORMAT.parse(profileJSON.getString(NetworkUtils.KEY_CREATED)));
            _profile.setLoggedIn(true);
            _profile.setLogin(true);
            p.saveProfile(_profile);

            GogglesApplication.getApplication().setLoggedIn(true);
            
            SharedPreferences.Editor preferencesEditor = PreferenceManager.getDefaultSharedPreferences(_context).edit();
            preferencesEditor.putBoolean(PreferencesUtils.KEY_AUTO_LOGIN, true);
            preferencesEditor.commit();
            
            Notifications.send(Notifications.LOGIN_CHANGED);
            
            super.onSuccess(response);

        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            onFailure(null, e.getMessage());
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            onFailure(null, e.getMessage());
        }
    }
    
    @Override
    public void onFailure(final String status, final String text) {
        super.onFailure(status, text);
        
        GogglesApplication.getApplication().setLoggedIn(false);
    }

    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlLoginUser();
    }
    
    @Override
    protected String getBody() {
        return NetworkUtils.buildLoginRequest(_profile.getUsername(), _profile.getPassword());
    }
}
