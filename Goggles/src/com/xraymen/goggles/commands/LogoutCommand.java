package com.xraymen.goggles.commands;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.xraymen.goggles.GogglesApplication;
import com.xraymen.goggles.Notifications;
import com.xraymen.goggles.PreferencesUtils;
import com.xraymen.goggles.db.Profile;
import com.xraymen.goggles.db.ProfileTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class LogoutCommand extends HttpCommandBase {

    private Profile _profile;

    /**
     * Logout a specific profile
     * @param context
     * @param profile
     * @param listener
     */
    public LogoutCommand(Context context, Profile profile, CompleteListener listener) {
        super(context, listener);
        _profile = profile;
    }

    /**
     * Logout current login profile
     * @param context
     * @param listener
     */
    public LogoutCommand(Context context, CompleteListener listener) {
        super(context, listener);
        ProfileTableHelper profileTableHelper = new ProfileTableHelper();
        _profile = profileTableHelper.getLoginProfile();
    }
    
    @Override
    public void execute() {
        GogglesApplication.getApplication().setLoggedIn(false);
        
        SharedPreferences.Editor preferencesEditor = PreferenceManager.getDefaultSharedPreferences(_context).edit();
        preferencesEditor.putBoolean(PreferencesUtils.KEY_AUTO_LOGIN, false);
        preferencesEditor.commit();
            
        if (_profile == null) {
            fireFailure("Not logged in");
        } else {
            super.execute();
        }
    }

    @Override
    public void onSuccess(JSONObject response) {
        _profile.setLoggedIn(false);
        ProfileTableHelper profileTableHelper = new ProfileTableHelper();
        profileTableHelper.saveProfile(_profile);
        super.onSuccess(response);
        
        Notifications.send(Notifications.LOGIN_CHANGED);
    }

    @Override
    protected String getBody() {
        return NetworkUtils.buildLogoutRequest(_profile.getUsername());
    }
    
    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlLogoutUser();
    }
}
