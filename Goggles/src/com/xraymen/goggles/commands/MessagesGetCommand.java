package com.xraymen.goggles.commands;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;

import com.xraymen.goggles.Notifications;
import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class MessagesGetCommand extends HttpCommandBase {

    private static final Logger LOGGER = Logger.getLogger(MessagesGetCommand.class.getName());

    private Location _currentLocation;
    private boolean _welcomeState = false;
    
    /**
     * 
     * @param context
     * @param currentLocation
     * @param welcomeState
     * @param listener
     */
    public MessagesGetCommand(Context context, Location currentLocation, boolean welcomeState, CompleteListener listener) {
        super(context, listener);
        _currentLocation = currentLocation;
        _welcomeState = welcomeState; 
    }
    
    @Override
    protected String getBody() {
        return NetworkUtils.buildGetMessagesRequest(
                _currentLocation.getLatitude(),
                _currentLocation.getLongitude(),
                _currentLocation.getAltitude(), _welcomeState, null);
    }
    
    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlGetMessages();
    }
    
    @Override
    public void onSuccess(final JSONObject response) {
        try {
            MessageTableHelper messageTableHelper = new MessageTableHelper();
            JSONArray messages;
            messages = response.getJSONArray(NetworkUtils.KEY_MESSAGES);
            
            //TODO: get and save profiles too!

            ArrayList<Message> oldMessages = messageTableHelper.getMessages();
            ArrayList<Message> newMessages = new ArrayList<Message>();
            HashSet<Long> newIds = new HashSet<Long>();
            
            for (int messageIndex = 0; messageIndex < messages.length(); messageIndex++) {
                JSONObject messageJson = messages.getJSONObject(messageIndex);
                Message message = NetworkUtils.messageFromJson(messageJson);
                message.setStatus(Message.STATUS_POSTED);
                newMessages.add(message);
                newIds.add(message.getGlobalId());
            }
            
            for(Message oldMessage : oldMessages) {
                if (oldMessage.getStatus() == Message.STATUS_POSTED && !newIds.contains(oldMessage.getGlobalId())) {
                    messageTableHelper.removeMessage(oldMessage);
                }
            }
            
            messageTableHelper.saveMessages(newMessages);
            
            Notifications.send(Notifications.MESSAGES_UPDATED);

            super.onSuccess(response);

        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            super.onFailure(null, e.getMessage());
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            super.onFailure(null, e.getMessage());
        }
    }
    
    @Override
    public void onFailure(String status, String text) {
        Notifications.send(Notifications.RETRIEVE_FAILED);
        super.onFailure(status, text);
    }
}
