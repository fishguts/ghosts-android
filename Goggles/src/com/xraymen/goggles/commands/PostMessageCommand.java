package com.xraymen.goggles.commands;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.xraymen.goggles.Notifications;
import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class PostMessageCommand extends HttpCommandBase {

    private static final Logger LOGGER = Logger.getLogger(PostMessageCommand.class.getName());
    
    private Message _message;
    
    /**
     * 
     * @param context
     * @param message
     * @param listener
     */
    public PostMessageCommand(Context context, Message message, CompleteListener listener) {
        super(context, listener);
        _message = message;
    }

    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlPostMessage();
    }
    
    @Override
    protected String getBody() {
        return NetworkUtils.buildPostMessageRequest(_message.getOwnerId(),
                _message.getText(), _message.getLatitude(),
                _message.getLongitude(), _message.getAltitude(),
                _message.getCreated(), _message.getExpiration());
    }
    
    
    @Override
    public void onSuccess(final JSONObject response) {
        try {
            JSONArray messages = response.getJSONArray(NetworkUtils.KEY_MESSAGES);
            NetworkUtils.mergeMessageFromJson(messages.getJSONObject(0), _message);
            _message.setStatus(Message.STATUS_POSTED);
            MessageTableHelper tableHelper = new MessageTableHelper();
            tableHelper.saveMessage(_message);
            
            Notifications.send(Notifications.MESSAGES_UPDATED);
            
            super.onSuccess(response);
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            super.onFailure(null, e.getMessage());
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            super.onFailure(null, e.getMessage());
        }
    }

    @Override
    public void onFailure(final String status, final String text) {
        _message.setStatus(Message.STATUS_ERROR);
        _message.setError(text);
        MessageTableHelper tableHelper = new MessageTableHelper();
        tableHelper.saveMessage(_message);
        
        Notifications.send(Notifications.MESSAGES_UPDATED);
        
        super.onFailure(status, text);
    }
}
