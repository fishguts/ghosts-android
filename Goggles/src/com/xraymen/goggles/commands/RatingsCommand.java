package com.xraymen.goggles.commands;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.MessageTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class RatingsCommand extends HttpCommandBase {
    
    private static final Logger LOGGER = Logger.getLogger(RatingsCommand.class.getName());

    private Message _message;
    private int _rating = 0;
    
    /**
     * 
     * @param context
     * @param message
     * @param rating
     * @param listener
     */
    public RatingsCommand(Context context, Message message, int rating, CompleteListener listener) {
        super(context, listener);
        
        _message = message;
        _rating = rating;
    }
    
    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlRateMessage();
    }
    
    @Override
    protected String getBody() {
        return NetworkUtils.buildRateMessageRequest(_message.getGlobalId(), _rating);
    }

    @Override
    public void onSuccess(final JSONObject response) {
        try {
            JSONArray messages = response.getJSONArray(NetworkUtils.KEY_MESSAGES);
            NetworkUtils.mergeMessageFromJson(messages.getJSONObject(0), _message);
            MessageTableHelper tableHelper = new MessageTableHelper();
            tableHelper.saveMessage(_message);
            super.onSuccess(response);
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            super.onFailure(null, e.getMessage());
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            super.onFailure(null, e.getMessage());
        }
    }
}
