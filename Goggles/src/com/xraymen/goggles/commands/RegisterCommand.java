package com.xraymen.goggles.commands;

import org.json.JSONObject;

import android.content.Context;

import com.xraymen.goggles.db.Profile;
import com.xraymen.goggles.db.ProfileTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class RegisterCommand extends HttpCommandBase {

    private Profile _profile;
    
    /**
     * 
     * @param context
     * @param profile
     * @param listener
     */
    public RegisterCommand(Context context, Profile profile, CompleteListener listener) {
        super(context, listener);
        _profile = profile;
    }

    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlRegisterUser();
    }
    
    @Override
    protected String getBody() {
        return NetworkUtils.buildRegisterRequest(_profile.getUsername(), _profile.getEmail(), _profile.getPassword());
    }
    
    @Override
    public void onSuccess(JSONObject response) {
        
        ProfileTableHelper p = new ProfileTableHelper();

        //If there's an old login profile disable its login status so we can make the new profile the login profile.
        Profile oldLoginProfile = p.getLoginProfile();
        if (oldLoginProfile != null) {
            oldLoginProfile.setLogin(false);
            p.saveProfile(oldLoginProfile);
        }

        _profile.setLogin(true);
        p.saveProfile(_profile);

        super.onSuccess(response);
    }
}
