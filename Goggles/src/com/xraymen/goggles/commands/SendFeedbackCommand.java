package com.xraymen.goggles.commands;

import android.content.Context;

import com.xraymen.goggles.db.Profile;
import com.xraymen.goggles.db.ProfileTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class SendFeedbackCommand extends HttpCommandBase {

    private Profile _profile;
    private String _text;

    /**
     * 
     * @param context
     * @param profile
     * @param text
     * @param listener
     */
    public SendFeedbackCommand(Context context, Profile profile, String text, CompleteListener listener) {
        super(context, listener);
        
        _profile = profile;
        _text = text;
    }

    /**
     * 
     * @param context
     * @param text
     * @param listener
     */
    public SendFeedbackCommand(Context context, String text, CompleteListener listener) {
        super(context, listener);
        
        ProfileTableHelper profileTableHelper = new ProfileTableHelper();
        _profile = profileTableHelper.getLoginProfile();
        _text = text;
    }
    
    @Override
    public void execute() {
        if (_profile == null) {
            onFailure(null, "No Profile");
        } else {
            super.execute();
        }
    }

    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlSendFeedback();
    }
    
    @Override
    protected String getBody() {
        return NetworkUtils.buildSendFeedbackRequest(_profile.getUsername(), _text);
    }
}
