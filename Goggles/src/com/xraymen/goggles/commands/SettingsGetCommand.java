package com.xraymen.goggles.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;

import com.xraymen.goggles.R;
import com.xraymen.goggles.db.Settings;
import com.xraymen.goggles.db.SettingsTableHelper;
import com.xraymen.goggles.net.NetworkUtils;

public class SettingsGetCommand extends HttpCommandBase {

    private static final Logger LOGGER = Logger.getLogger(SettingsGetCommand.class.getName());

    /**
     * Constructor
     * @param context
     * @param listener
     */
    public SettingsGetCommand(Context context, CompleteListener listener) {
        super(context, listener);
    }
    
    @Override
    public void onSuccess(final JSONObject response) {
        try {
            SettingsTableHelper helper = new SettingsTableHelper();
            JSONArray settingsObjects = response.getJSONArray(NetworkUtils.KEY_SETTINGS);
            for (int i = 0; i < settingsObjects.length(); i++) {
                Settings settings = NetworkUtils.settingsFromJson(settingsObjects.getJSONObject(i));
                helper.saveSettings(settings);
            }
            
            JSONObject server = response.getJSONObject(NetworkUtils.KEY_SERVER);
            Editor editor = PreferenceManager.getDefaultSharedPreferences(_context).edit();
            editor.putString(_context.getString(R.string.prefs_serverVersionKey), server.getString(NetworkUtils.KEY_VERSION));
            editor.commit();
            
            super.onSuccess(response);
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            super.onFailure(null, e.getMessage());
        }
    }

    @Override
    protected String getBody() {
        String version = null;
        try {
            PackageInfo packageInfo;
            packageInfo = _context.getPackageManager().getPackageInfo(_context.getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (NameNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return NetworkUtils.buildGetSettingsRequest(version);
    }
    
    @Override
    protected String getUrl() {
        return NetworkUtils.getUrlGetSettings();
    }
}
