package com.xraymen.goggles.db;

import java.util.Date;

public class Message {
    
    public static final int STATUS_UNKNOWN = 0;
    public static final int STATUS_POSTED = 1;
    public static final int STATUS_ERROR = -1;
    
    private long _localId;
    private long _globalId;
    private Date _created;
    private String _text = "";
    private double _altitude;
    private double _longitude;
    private boolean _supportsFeedback;
    private Date _expiration;
    private int _ratingDislikes;
    private double _latitude;
    private String _ownerId;
    private String _type = "";
    private int _ratingLikes;
    private int _ratingUser;
    private boolean _flaggedUser;
    private int _color;
    private String _imageUrlFull = "";
    private String _imageUrlThumb = "";
    private int _ratingSum;
    private int _status = STATUS_UNKNOWN;
    private String _error = "";
    
    /**
     * 
     */
    public Message() {
        
    }

    public long getLocalId() {
        return _localId;
    }

    public void setLocalId(long _id) {
        this._localId = _id;
    }

    public long getGlobalId() {
        return _globalId;
    }

    public void setGlobalId(long messageId) {
        _globalId = messageId;   
    }


    public Date getCreated() {
        return _created;
    }

    public void setCreated(Date created) {
        this._created = created;
    }

    public String getText() {
        return _text;
    }

    public void setText(String text) {
        this._text = text;
    }

    public double getAltitude() {
        return _altitude;
    }

    public void setAltitude(double altitude) {
        this._altitude = altitude;
    }

    public double getLongitude() {
        return _longitude;
    }

    public void setLongitude(double longitude) {
        this._longitude = longitude;
    }

    public boolean isSupportsFeedback() {
        return _supportsFeedback;
    }

    public void setSupportsFeedback(boolean supportsFeedback) {
        this._supportsFeedback = supportsFeedback;
    }

    public Date getExpiration() {
        return _expiration;
    }

    public void setExpiration(Date expiration) {
        this._expiration = expiration;
    }

    public int getRatingDislikes() {
        return _ratingDislikes;
    }

    public void setRatingDislikes(int ratingDislikes) {
        this._ratingDislikes = ratingDislikes;
    }

    public double getLatitude() {
        return _latitude;
    }

    public void setLatitude(double latitude) {
        this._latitude = latitude;
    }

    public String getOwnerId() {
        return _ownerId;
    }

    public void setOwnerId(String ownerId) {
        this._ownerId = ownerId;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        this._type = type;
    }

    public int getRatingLikes() {
        return _ratingLikes;
    }

    public void setRatingLikes(int ratingLikes) {
        this._ratingLikes = ratingLikes;
    }

    public int getRatingUser() {
        return _ratingUser;
    }

    public void setRatingUser(int ratingUser) {
        this._ratingUser = ratingUser;
    }

    public boolean getFlaggedUser() {
        return _flaggedUser;
    }

    public void setFlaggedUser(boolean flaggedUser) {
        this._flaggedUser = flaggedUser;
    }

    public int getColor() {
        return _color;
    }

    public void setColor(int color) {
        this._color = color;
    }

    public String getImageUrlFull() {
        return _imageUrlFull;
    }

    public void setImageUrlFull(String imageUrlFull) {
        this._imageUrlFull = imageUrlFull;
    }

    public String getImageUrlThumb() {
        return _imageUrlThumb;
    }

    public void setImageUrlThumb(String imageUrlThumb) {
        this._imageUrlThumb = imageUrlThumb;
    }

    public int getRatingSum() {
        return _ratingSum;
    }

    public void setRatingSum(int ratingSum) {
        this._ratingSum = ratingSum;
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        this._status = status;
    }

    public String getError() {
        return _error;
    }

    public void setError(String error) {
        this._error = error;
    }

    /**
     * 
     * @return
     */
    public String getSortPrefix() {
        String s;
        if ("ad".equalsIgnoreCase(_type)) {
            s = "a";
        } else if ("welcome".equalsIgnoreCase(_type)) {
            s = "b";
        } else if ("notification".equalsIgnoreCase(_type)) {
            s = "c";
        } else {
            s = "d";
        }

        if (Message.STATUS_UNKNOWN == _status) {
            s += "a";
        } else if (Message.STATUS_ERROR  == _status) {
            s += "b";
        } else {
            s += "c";
        }
        return s;
    }
}
