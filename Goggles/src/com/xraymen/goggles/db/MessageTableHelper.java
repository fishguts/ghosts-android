package com.xraymen.goggles.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MessageTableHelper extends OpenHelper implements Table {

    private final static Logger LOGGER = Logger.getLogger(MessageTableHelper.class.getName());
    
    public final static String TABLE_NAME = "Messages";
    
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_MESSAGEID = "messageId";
    public static final String COLUMN_CREATED = "created";
    public static final String COLUMN_TEXT = "text";
    public static final String COLUMN_ALTITUDE = "altitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_SUPPORTSFEEDBACK = "supportsFeedback";
    public static final String COLUMN_EXPIRATION = "expiration";
    public static final String COLUMN_RATINGDISLIKES = "ratingDislikes";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_OWNERID = "ownerId";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_RATINGLIKES = "ratingLikes";
    public static final String COLUMN_RATINGUSER = "ratingUser";
    public static final String COLUMN_FLAGGEDUSER = "flaggedUser";
    public static final String COLUMN_COLOR = "color";
    public static final String COLUMN_IMAGEURLFULL = "imageUrlFull";
    public static final String COLUMN_IMAGEURLTHUMB = "imageUrlThumb";
    public static final String COLUMN_RATINGSUM = "ratingSum";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_ERROR = "error";
    public static final String COLUMN_SORTPREFIX = "sortPrefix";
    
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_MESSAGEID + " INTEGER, "
            + COLUMN_CREATED + " TIMESTAMP, " + COLUMN_TEXT + " TEXT, "
            + COLUMN_ALTITUDE + " FLOAT, " + COLUMN_LONGITUDE + " FLOAT, "
            + COLUMN_SUPPORTSFEEDBACK + " BOOL, " 
            + COLUMN_EXPIRATION + " TIMESTAMP, " + COLUMN_RATINGDISLIKES + " INTEGER, "
            + COLUMN_LATITUDE + " FLOAT, " + COLUMN_OWNERID + " TEXT, "
            + COLUMN_TYPE + " TEXT, " + COLUMN_RATINGLIKES + " INTEGER, "
            + COLUMN_RATINGUSER + " INTEGER, " + COLUMN_FLAGGEDUSER + " TEXT, "
            + COLUMN_COLOR + " INTEGER, " + COLUMN_IMAGEURLFULL + " TEXT, "
            + COLUMN_IMAGEURLTHUMB + " TEXT, " 
            + COLUMN_RATINGSUM + " INTEGER, " + COLUMN_STATUS + " INTEGER, " 
            + COLUMN_ERROR + " TEXT, " 
            + COLUMN_SORTPREFIX + " TEXT " + ");";

    private static final String ORDERBY_CREATED = COLUMN_SORTPREFIX + " ASC, " + 
            COLUMN_CREATED + " DESC";

    private static final String ORDERBY_RATINGS = COLUMN_SORTPREFIX + " ASC, " + 
            COLUMN_RATINGSUM + " DESC, " + 
            COLUMN_CREATED + " DESC";
    
    public static final int SORT_CREATED = 0;
    public static final int SORT_RATING = 1;
    
    public MessageTableHelper() {
    }

    @Override
    public void createTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable(db);
    }

    /**
     * 
     * @param messages
     */
    public void saveMessages(ArrayList<Message> messages) {

        SQLiteDatabase db = getWritableDatabase();

        for (Message message : messages) {
            saveMessage(db, message);
        }
        db.close();
    }

    /**
     * 
     * @param message
     */
    public void saveMessage(Message message) {
        SQLiteDatabase db = getWritableDatabase();
        saveMessage(db, message);
        db.close();
    }

    /**
     * 
     * @return
     */
    public ArrayList<Message> getMessages() {
        return getMessages(null, null, ORDERBY_CREATED);
    }
    
    /**
     * 
     * @param sortOrder SORT_RATING or SORT_CREATED
     * @return
     */
    public ArrayList<Message> getMessages(int sortOrder) {
        return getMessages(null, null, sortOrder == SORT_RATING ? ORDERBY_RATINGS : ORDERBY_CREATED);
    }
    
    /**
     * 
     * @return
     */
    public ArrayList<Message> getFailedMessages() {
        return getMessages(COLUMN_STATUS + " = ?", new String[] {Integer.toString(Message.STATUS_ERROR)}, ORDERBY_CREATED);
    }

    /**
     * 
     * @param messageId
     * @return
     */
    public Message getMessage(long messageId) {
        
        ArrayList<Message> messages = getMessages(COLUMN_ID + " = ?", new String[] { Long.toString(messageId) }, ORDERBY_CREATED);

        Message message = null;
        if (messages.size() > 0) {
            message = messages.get(0);
        }
        return message;
    }
    
    /**
     * 
     * @param selection
     * @param selectionArgs
     * @param orderBy
     * @return
     */
    private ArrayList<Message> getMessages(final String selection, final String [] selectionArgs, String orderBy) {
        SQLiteDatabase db = getReadableDatabase();
        
        ArrayList<Message> messages = new ArrayList<Message>();
        
        Cursor cursor = db.query(TABLE_NAME, null, selection, selectionArgs, null, null, orderBy);
        
        while(cursor.moveToNext()) {
            messages.add(cursorToMessage(cursor));
        }
        
        cursor.close();
        db.close();

        return messages;
    }

    /**
     * 
     * @param message
     */
    public void removeMessage(final Message message) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + " = ?",
                new String[] { Long.toString(message.getLocalId()) });
        db.close();
    }

    /**
     * 
     * @param cursor
     * @return
     */
    private Message cursorToMessage(final Cursor cursor) {
        Message message = new Message();
        message.setLocalId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
        message.setGlobalId(cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGEID)));
        message.setCreated(new Date(cursor.getLong(cursor.getColumnIndex(COLUMN_CREATED)))); 
        message.setText(cursor.getString(cursor.getColumnIndex(COLUMN_TEXT)));
        message.setAltitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_ALTITUDE)));
        message.setLongitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_LONGITUDE)));
        message.setSupportsFeedback(cursor.getInt(cursor.getColumnIndex(COLUMN_SUPPORTSFEEDBACK)) == 1);
        long expiration = cursor.getLong(cursor.getColumnIndex(COLUMN_EXPIRATION));
        if (expiration != 0) {
            message.setExpiration(new Date(expiration));
        }
        message.setRatingDislikes(cursor.getInt(cursor.getColumnIndex(COLUMN_RATINGDISLIKES)));
        message.setLatitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_LATITUDE)));
        message.setOwnerId(cursor.getString(cursor.getColumnIndex(COLUMN_OWNERID)));
        message.setType(cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)));
        message.setRatingLikes(cursor.getInt(cursor.getColumnIndex(COLUMN_RATINGLIKES)));
        message.setRatingUser(cursor.getInt(cursor.getColumnIndex(COLUMN_RATINGUSER)));
        message.setFlaggedUser(cursor.getInt(cursor.getColumnIndex(COLUMN_FLAGGEDUSER))==1);
        message.setColor(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR)));
        message.setImageUrlFull(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGEURLFULL)));
        message.setImageUrlThumb(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGEURLTHUMB) ));
        message.setRatingSum(cursor.getInt(cursor.getColumnIndex(COLUMN_RATINGSUM) ));
        message.setStatus(cursor.getInt(cursor.getColumnIndex(COLUMN_STATUS) ));
        message.setError(cursor.getString(cursor.getColumnIndex(COLUMN_ERROR) ));
        return message;
    }
    
    /**
     * 
     * @param db
     * @param message
     */
    private void saveMessage(SQLiteDatabase db, Message message) {

        ContentValues cv = new ContentValues();
        if (message.getLocalId() != 0) {
            cv.put(COLUMN_ID, message.getLocalId());
        }
        cv.put(COLUMN_MESSAGEID, message.getGlobalId());
        Date created = message.getCreated();
        if (created != null) {
            cv.put(COLUMN_CREATED, created.getTime());
        }
        cv.put(COLUMN_TEXT, message.getText());
        cv.put(COLUMN_ALTITUDE, message.getAltitude());
        cv.put(COLUMN_LONGITUDE, message.getLongitude());
        cv.put(COLUMN_SUPPORTSFEEDBACK, message.isSupportsFeedback());
        Date expiration = message.getExpiration();
        if (expiration != null) {
            cv.put(COLUMN_EXPIRATION, expiration.getTime());
        }
        cv.put(COLUMN_RATINGDISLIKES, message.getRatingDislikes());
        cv.put(COLUMN_LATITUDE, message.getLatitude());
        cv.put(COLUMN_OWNERID, message.getOwnerId());
        cv.put(COLUMN_TYPE, message.getType());
        cv.put(COLUMN_RATINGLIKES, message.getRatingLikes());
        cv.put(COLUMN_RATINGUSER, message.getRatingUser());
        cv.put(COLUMN_FLAGGEDUSER, message.getFlaggedUser());
        cv.put(COLUMN_COLOR, message.getColor());
        cv.put(COLUMN_IMAGEURLFULL, message.getImageUrlFull());
        cv.put(COLUMN_IMAGEURLTHUMB, message.getImageUrlThumb());
        cv.put(COLUMN_RATINGSUM, message.getRatingSum());
        cv.put(COLUMN_STATUS, message.getStatus());
        cv.put(COLUMN_ERROR, message.getError());
        cv.put(COLUMN_SORTPREFIX, message.getSortPrefix());

        Cursor cursor = null;
        String selection;
        String [] selectionArgs;

        if (message.getStatus() == Message.STATUS_POSTED && message.getLocalId() == 0) {
            selection = COLUMN_MESSAGEID + " = ?";
            selectionArgs = new String[]{Long.toString(message.getGlobalId())};
        } else {
            selection = COLUMN_ID + " = ?";
            selectionArgs = new String[]{Long.toString(message.getLocalId())};
        }

        cursor = db.query(TABLE_NAME,
                null, selection, selectionArgs, null,
                null, null);

        if (cursor != null && cursor.getCount() > 0) {
            //Just for logging
            cursor.moveToFirst();
            LOGGER.info(String.format("Updating message %d at row %d: %s", cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGEID)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_ID)), message.getText()));
            ///////
            db.update(TABLE_NAME, cv, selection, selectionArgs);
        } else {
            long id = db.insert(TABLE_NAME, null, cv);
            LOGGER.info(String.format("Inserting message %d at row %d: %s", message.getGlobalId(), id, message.getText()));
            message.setLocalId(id);
        }

        if (cursor != null) {
            cursor.close();
        }
    }
}
