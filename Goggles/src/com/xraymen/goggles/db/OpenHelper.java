package com.xraymen.goggles.db;

import com.xraymen.goggles.GogglesApplication;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelper extends SQLiteOpenHelper {
    
    final static private String DB_NAME = "GogglesDB";
    private static final int DB_VERSION = 3;

    public OpenHelper() {
        super(GogglesApplication.getContext(), DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Table [] tables = getTables();
        for (Table table : tables) {
            table.createTable(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Table [] tables = getTables();
        for (Table table : tables) {
            table.upgradeTable(db, oldVersion, newVersion);
        }
    }

    /**
     * Get an array of Table objects which are in the Goggles database. Any new Table must be added to this array.
     * @return
     */
    private static Table [] getTables() {
        return new Table [] {
                new ProfileTableHelper(), 
                new MessageTableHelper(),
                new SettingsTableHelper()
                };
    }
}
