package com.xraymen.goggles.db;

import java.util.Date;

public class Profile  {

    private Date _created;
    private String _email = "";
    private String _imageUrlFull = "";
    private String _imageUrlThumb = "";
    private boolean _loggedIn = false;
    private boolean _login = false;
    private String _password = "";
    private String _status = "";
    private String _username = "";

    /**
     * 
     */
    public Profile() {
        
    }

    public Date getCreated() {
        return _created;
    }

    public void setCreated(Date _created) {
        this._created = _created;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public String getImageUrlFull() {
        return _imageUrlFull;
    }

    public void setImageUrlFull(String _imageUrlFull) {
        this._imageUrlFull = _imageUrlFull;
    }

    public String getImageUrlThumb() {
        return _imageUrlThumb;
    }

    public void setImageUrlThumb(String _imageUrlThumb) {
        this._imageUrlThumb = _imageUrlThumb;
    }

    public boolean isLoggedIn() {
        return _loggedIn;
    }

    public void setLoggedIn(boolean _loggedIn) {
        this._loggedIn = _loggedIn;
    }

    public boolean isLogin() {
        return _login;
    }

    public void setLogin(boolean _login) {
        this._login = _login;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String _password) {
        this._password = _password;
    }

    public String getStatus() {
        return _status;
    }

    public void setStatus(String _status) {
        this._status = _status;
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String _username) {
        this._username = _username;
    }

}
