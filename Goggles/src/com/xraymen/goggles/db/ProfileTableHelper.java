package com.xraymen.goggles.db;

import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ProfileTableHelper extends OpenHelper implements Table {

    private static final String TABLE_NAME = "Profiles";

    public final static String COLUMN_ID = "_id";
    public final static String COLUMN_CREATED = "created";
    public final static String COLUMN_EMAIL = "email";
    public final static String COLUMN_IMAGEURLFULL = "imageUrlFull";
    public final static String COLUMN_IMAGEURLTHUMB = "imageUrlThumb";
    public final static String COLUMN_LOGGEDIN = "loggedIn";
    public final static String COLUMN_LOGIN = "login";
    public final static String COLUMN_PASSWORD = "password";
    public final static String COLUMN_STATUS = "status";
    public final static String COLUMN_USERNAME = "username";
    
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_CREATED
            + " TIMESTAMP, " + COLUMN_EMAIL + " TEXT, " + COLUMN_IMAGEURLFULL
            + " TEXT, " + COLUMN_IMAGEURLTHUMB + " TEXT, " + COLUMN_LOGGEDIN
            + " BOOL, " + COLUMN_LOGIN + " BOOL, " + COLUMN_PASSWORD
            + " TEXT, " + COLUMN_STATUS + " TEXT, " + COLUMN_USERNAME + " TEXT"
            + ");";
    
    public ProfileTableHelper() {
    }

    @Override
    public void createTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable(db);
    }

    /**
     * 
     * @return
     */
    public ArrayList<Profile> getProfiles() {
        return getProfiles();
    }
    
    /**
     * 
     * @param name
     * @return
     */
    public Profile getProfileByName(final String name) {
        ArrayList<Profile> profiles = getProfiles(COLUMN_USERNAME + " = ?", new String[]{name});
        Profile p = null;
        if (profiles.size() > 0) {
            p = profiles.get(0);
        }
        return p;
    }
    
    /**
     * 
     * @return
     */
    public Profile getLoginProfile() {
        ArrayList<Profile> profiles = getProfiles(COLUMN_LOGIN + " = ?", new String[]{Integer.toString(1)});
        Profile p = null;
        if (profiles.size() > 0) {
            p = profiles.get(0);
        }
        return p;
    }
    
    /**
     * 
     * @param selection
     * @param selectionArgs
     * @return
     */
    private ArrayList<Profile> getProfiles(String selection, String [] selectionArgs) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Profile> profiles = new ArrayList<Profile>();
        
        Cursor cursor = db.query(TABLE_NAME,
                null, selection, selectionArgs, null,
                null, null);

        while(cursor.moveToNext()) {
            profiles.add(cursorToProfile(cursor));
        }

        cursor.close();
        db.close();

        return profiles;
    }

    /**
     * 
     * @param cursor
     * @return
     */
    private Profile cursorToProfile(final Cursor cursor) {
        Profile profile = new Profile();
        
        profile.setCreated(new Date(cursor.getLong(cursor.getColumnIndex(COLUMN_CREATED))));
        profile.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)));
        profile.setImageUrlFull(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGEURLFULL)));
        profile.setImageUrlThumb(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGEURLTHUMB)));
        profile.setLoggedIn(cursor.getInt(cursor.getColumnIndex(COLUMN_LOGGEDIN)) == 1);
        profile.setLogin(cursor.getInt(cursor.getColumnIndex(COLUMN_LOGIN)) == 1);
        profile.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD)));
        profile.setStatus(cursor.getString(cursor.getColumnIndex(COLUMN_STATUS)));
        profile.setUsername(cursor.getString(cursor.getColumnIndex(COLUMN_USERNAME)));

        return profile;
    }
    
    /**
     * 
     * @param profile
     */
    public void saveProfile(final Profile profile) {

        ContentValues contentValues = new ContentValues();
        Date created = profile.getCreated();
        if (created != null) {
            contentValues.put(COLUMN_CREATED, created.getTime());
        }
        contentValues.put(COLUMN_EMAIL, profile.getEmail());
        contentValues.put(COLUMN_IMAGEURLFULL, profile.getImageUrlFull());
        contentValues.put(COLUMN_IMAGEURLTHUMB, profile.getImageUrlThumb());
        contentValues.put(COLUMN_LOGGEDIN, profile.isLoggedIn());
        contentValues.put(COLUMN_LOGIN, profile.isLogin());
        contentValues.put(COLUMN_PASSWORD, profile.getPassword());
        contentValues.put(COLUMN_STATUS, profile.getStatus());
        contentValues.put(COLUMN_USERNAME, profile.getUsername());

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                null, COLUMN_USERNAME + " = ?", new String[]{profile.getUsername()}, null,
                null, null);

        if (cursor.getCount() > 0) {
            db.update(TABLE_NAME, contentValues, COLUMN_USERNAME + " = ?", new String[]{profile.getUsername()});
        } else {
            db.insert(TABLE_NAME, null, contentValues);
        }

        cursor.close();
        db.close();
    }
}
