package com.xraymen.goggles.db;

public class Settings {

    public static final int DEFAULT_QUERY_INTERVAL_MAX = 60;
    public static final int DEFAULT_QUERY_INTERVAL_MIN = 15;
    public static final int DEFAULT_RADIUS_HORIZONTAL = 75;
    public static final int DEFAULT_RADIUS_VERTICAL = 75;
    public static final int DEFAULT_MESSAGES_PER_QUERY = 25;
    
    private String _name = "";
    private int _queryIntervalMin = DEFAULT_QUERY_INTERVAL_MAX;
    private int _queryIntervalMax = DEFAULT_QUERY_INTERVAL_MAX;
    private double _radiusHorizontal = DEFAULT_RADIUS_HORIZONTAL;
    private double _radiusVertical = DEFAULT_RADIUS_VERTICAL;
    private int _messagesPerQuery = DEFAULT_MESSAGES_PER_QUERY;
    
    public String getName() {
        return _name;
    }

    public void setName(String Name) {
        _name = Name;
    }

    public int getQueryIntervalMin() {
        return _queryIntervalMin;
    }

    public void setQueryIntervalMin(int queryIntervalMin) {
        _queryIntervalMin = queryIntervalMin;
    }

    public int getQueryIntervalMax() {
        return _queryIntervalMax;
    }

    public void setQueryIntervalMax(int queryIntervalMax) {
        _queryIntervalMax = queryIntervalMax;
    }

    public double getRadiusHorizontal() {
        return _radiusHorizontal;
    }

    public void setRadiusHorizontal(double radiusHorizontal) {
        _radiusHorizontal = radiusHorizontal;
    }

    public double getRadiusVertical() {
        return _radiusVertical;
    }

    public void setRadiusVertical(double radiusVertical) {
        _radiusVertical = radiusVertical;
    }

    public int getMessagesPerQuery() {
        return _messagesPerQuery;
    }

    public void setMessagesPerQuery(int messagesPerQuery) {
        _messagesPerQuery = messagesPerQuery;
    }
}
