package com.xraymen.goggles.db;

import java.util.ArrayList;
import java.util.logging.Logger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SettingsTableHelper extends OpenHelper implements Table {

    private static final String DEFAULT_SETTING = "radial";

    private final static Logger LOGGER = Logger.getLogger(SettingsTableHelper.class.getName());
    
    public final static String TABLE_NAME = "Settings";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_QUERY_INTERVAL_MAX = "queryIntervalMax";
    public static final String COLUMN_QUERY_INTERVAL_MIN = "queryIntervalMin";
    public static final String COLUMN_RADIUS_HORIZONTAL = "radiusHorizontal";
    public static final String COLUMN_RADIUS_VERTICAL = "radiusVertical";
    public static final String COLUMN_MESSAGES_PER_QUERY = "messagesPerQueryMax";

    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NAME + " TEXT, " + COLUMN_QUERY_INTERVAL_MAX + " INTEGER, " + COLUMN_QUERY_INTERVAL_MIN + " INTEGER, "
            + COLUMN_RADIUS_HORIZONTAL + " FLOAT, " + COLUMN_RADIUS_VERTICAL + " FLOAT, " + COLUMN_MESSAGES_PER_QUERY + " INTEGER " + ");";
    
    @Override
    public void createTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable(db);
    }

    /**
     * 
     * @param settingsList
     */
    public void saveSettingsList(ArrayList<Settings> settingsList) {
        SQLiteDatabase db = getWritableDatabase();
        for(Settings settings : settingsList) {
            saveSettings(db, settings);
        }
        db.close();
    }

    /**
     * 
     * @param settings
     */
    public void saveSettings(Settings settings) {
        SQLiteDatabase db = getWritableDatabase();
        saveSettings(db, settings);
        db.close();
    }
    
    /**
     * 
     * @param db
     * @param settings
     */
    private void saveSettings(SQLiteDatabase db, Settings settings) {
        
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, settings.getName());
        cv.put(COLUMN_QUERY_INTERVAL_MAX, settings.getQueryIntervalMax());
        cv.put(COLUMN_QUERY_INTERVAL_MIN, settings.getQueryIntervalMin());
        cv.put(COLUMN_RADIUS_HORIZONTAL, settings.getRadiusHorizontal());
        cv.put(COLUMN_RADIUS_VERTICAL, settings.getRadiusVertical());
        cv.put(COLUMN_MESSAGES_PER_QUERY, settings.getMessagesPerQuery());
        
        String whereClause = COLUMN_NAME + "=?";
        String[] whereArgs = new String[]{settings.getName()};
        
        Cursor cursor = db.query(TABLE_NAME, null, whereClause, whereArgs, null, null, null);
        if (cursor.getCount() > 0) {
            LOGGER.fine("Updating settings " + settings.getName());
            db.update(TABLE_NAME, cv, whereClause, whereArgs);
        } else {
            LOGGER.fine("Inserting settings " + settings.getName());
            db.insert(TABLE_NAME, null, cv);
        }
        cursor.close();
    }

    /**
     * 
     * @return
     */
    public ArrayList<Settings> getAllSettings() {
        ArrayList<Settings> result = new ArrayList<Settings>();
        
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        while(cursor.moveToNext()) {
            result.add(cursorToSettings(cursor));
        }
        cursor.close();
        db.close();

        return result;
    }

    /**
     * 
     * @param name
     * @return
     */
    public Settings getSettingsByName(final String name) {
        Settings settings = null;
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, null, COLUMN_NAME + "=?", new String[]{name}, null, null, null);
        if (cursor.moveToFirst()) {
            settings = cursorToSettings(cursor);
        }
        cursor.close();
        db.close();

        return settings;
    }
    
    /**
     * Get the current settings object.
     * @return
     */
    public Settings getSettings() {
        Settings settings = getSettingsByName(DEFAULT_SETTING);
        if (settings == null) {
            settings = new Settings();
        }
        return settings;
    }

    /**
     * 
     * @param cursor
     * @return
     */
    public Settings cursorToSettings(Cursor cursor) {
        Settings settings = new Settings();
        settings.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
        settings.setQueryIntervalMax(cursor.getInt(cursor.getColumnIndex(COLUMN_QUERY_INTERVAL_MAX)));
        settings.setQueryIntervalMin(cursor.getInt(cursor.getColumnIndex(COLUMN_QUERY_INTERVAL_MIN)));
        settings.setRadiusHorizontal(cursor.getDouble(cursor.getColumnIndex(COLUMN_RADIUS_HORIZONTAL)));
        settings.setRadiusVertical(cursor.getDouble(cursor.getColumnIndex(COLUMN_RADIUS_VERTICAL)));
        settings.setMessagesPerQuery(cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGES_PER_QUERY)));
        return settings;
    }
}
