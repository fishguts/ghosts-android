package com.xraymen.goggles.db;

import android.database.sqlite.SQLiteDatabase;

public interface Table {
    
    public void createTable(SQLiteDatabase db);
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion);
}
