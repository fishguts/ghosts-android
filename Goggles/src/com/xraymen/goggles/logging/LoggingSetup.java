package com.xraymen.goggles.logging;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.xraymen.goggles.GogglesApplication;

import android.util.Log;

/**
 * 
 * @author amitlissack
 * 
 */
public class LoggingSetup {

    /**
     * Minimum debug level.
     */
    private static final Level LEVEL = Level.SEVERE;

    private static final String TAG = LoggingSetup.class.getName();

    private static final Formatter FORMATTER = new SingleLineFormatter();

    /**
     * 
     */
    public static void setup() {
        Logger logger = Logger.getLogger("com.xraymen.goggles");
        try {
            // Create log file in xraymen directory on sd card
            File logDirectory = new File(GogglesApplication.getContext().getExternalFilesDir(null), "log.txt");
            FileHandler fileHandler = new FileHandler(logDirectory.getPath(), true);
            fileHandler.setFormatter(FORMATTER);
            logger.addHandler(fileHandler);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        logger.setLevel(LEVEL);
    }
}
