package com.xraymen.goggles.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SingleLineFormatter extends Formatter {

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    
    private final static String SEPARATOR = " ";
    
    @Override
    public String format(LogRecord r) {
        StringBuffer sb = new StringBuffer();
        sb.append(DATE_FORMAT.format(new Date(r.getMillis())));
        sb.append(SEPARATOR);
        
        sb.append(r.getLevel());
        sb.append(SEPARATOR);
        
        sb.append(r.getLoggerName());
        sb.append(SEPARATOR);
        
        sb.append(formatMessage(r));
        sb.append("\n");
        
        return sb.toString();
    }

}
