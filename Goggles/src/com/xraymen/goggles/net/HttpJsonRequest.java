package com.xraymen.goggles.net;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;

/**
 * 
 * @author amitlissack
 * 
 */
public class HttpJsonRequest {

    static private final Logger LOGGER = Logger.getLogger(HttpJsonRequest.class.getName());
    
    private Context _context;
    private int _verb;
    protected String _url;
    protected String _body;
    private JsonResponseListener _listener;

    /**
     * 
     * @param context
     * @param verb
     * @param url
     * @param body
     * @param listener
     */
    public HttpJsonRequest(Context context, final int verb, final String url, final String body, JsonResponseListener listener) {
        _context = context;
        _verb = verb;
        _url = url;
        _body = body;
        _listener = listener;
    }

    /**
     * 
     * @param context
     * @param url
     * @param body
     * @param listener
     */
    public HttpJsonRequest(Context context, final String url, final String body, JsonResponseListener listener) {
        this(context, HttpRequestService.POST, url, body, listener);
    }

    /**
     * 
     * @param context
     * @param url
     * @param listener
     */
    public HttpJsonRequest(Context context, final String url, JsonResponseListener listener) {
        this(context, HttpRequestService.GET, url, null, listener);
    }
    
    public void execute() {
        Intent intent = new Intent(_context, HttpRequestService.class);
        intent.setData(Uri.parse(_url));
        intent.putExtra(HttpRequestService.EXTRA_RECEIVER, new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null) {
                    if (resultCode == 1) {
                        String body = resultData.getString(HttpRequestService.RESPONSE_KEY_DATA);

                        try {
                            // Parse the JSON
                            JSONObject j;
                            j = new JSONObject(body);
                            String messageStatus = j.getJSONObject(NetworkUtils.KEY_STATUS).getString(NetworkUtils.KEY_CODE);
                            if (NetworkUtils.VALUE_OK.equals(messageStatus)) {
                                _listener.onSuccess(j);
                            } else {
                                _listener.onFailure(messageStatus, j.getJSONObject(NetworkUtils.KEY_STATUS)
                                        .getString(NetworkUtils.KEY_TEXT));
                            }
                        } catch (JSONException e) {
                            LOGGER.log(Level.SEVERE, e.getMessage(), e);
                            _listener.onFailure(null, e.getMessage());
                        }

                    } else {
                        String error = resultData.getString(HttpRequestService.RESPONSE_KEY_ERROR);
                        _listener.onFailure("", error);
                    }
                } else {
                    _listener.onFailure("", "");
                }
            }
        });
        intent.putExtra(HttpRequestService.EXTRA_DATA, _body);
        intent.putExtra(HttpRequestService.EXTRA_VERB, _verb);
        _context.startService(intent);
        
    }

    /**
     * 
     * @author amitlissack
     *
     */
    public interface JsonResponseListener {
        public void onSuccess(final JSONObject response);

        public void onFailure(final String status, final String text);
    }
}
