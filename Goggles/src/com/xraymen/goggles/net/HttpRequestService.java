package com.xraymen.goggles.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.xraymen.goggles.R;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;

public class HttpRequestService extends IntentService {

    public HttpRequestService() {
        super(HttpRequestService.class.getName());
    }

    public static final Logger LOGGER = Logger.getLogger(HttpRequestService.class.getName());

    public static final String EXTRA_RECEIVER = "receiver";
    public static final String EXTRA_DATA = "data";
    public static final String EXTRA_VERB = "verb";

    public static final String RESPONSE_KEY_DATA = "data";
    public static final String RESPONSE_KEY_ERROR = "error";
    
    public static final int GET = 0;
    public static final int PUT = 1;
    public static final int POST = 2;
    public static final int DELETE = 3;

    private static DefaultHttpClient _client = null;
    
    private static final int SOCKET_TIMEOUT = 20000;

    private static final int CONNECTION_TIMEOUT = 20000;
    
    @Override
    public void onCreate() {
        super.onCreate();

        LOGGER.info("onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LOGGER.info("onDestroy");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Uri uri = intent.getData();

        Bundle extras = intent.getExtras();
        int _verb = extras.getInt(EXTRA_VERB);
        String _body = extras.getString(EXTRA_DATA);
        ResultReceiver _receiver = extras.getParcelable(EXTRA_RECEIVER);
        String _url = uri.toString();
        
        if (_client == null) {
            _client = new DefaultHttpClient();
            //Set the timeout
            final HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);
            _client.setParams(params);
        }
        
        HttpUriRequest httpRequest = null;
        HttpResponse result = null;

        try {
            switch (_verb) {
            case (GET):
                httpRequest = new HttpGet(_url);
                break;
            case (POST):
                HttpPost post = new HttpPost(_url);
                post.setEntity(new StringEntity(_body));
                httpRequest = post;
                break;
            case (PUT):
                HttpPut put = new HttpPut(_url);
                put.setEntity(new StringEntity(_body));
                httpRequest = put;
                break;
            case (DELETE):
                httpRequest = new HttpDelete(_url);
                break;
            default:
                LOGGER.info(String.format("%d is an invalid verb", _verb));
                return;
            }

            httpRequest.addHeader("User-Agent", "Goggles/Android");
            httpRequest.addHeader("Content-Type", "application/json");

            LOGGER.info(String.format("Url: %s. Body: %s", _url, _body));

            result = _client.execute(httpRequest);
            
            if (result == null || result.getStatusLine() == null) {
                sendResponse(_receiver, false, "NULL response", null);
                return;
            }

            if (result.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                sendResponse(_receiver, false, result.getStatusLine().getReasonPhrase(), null);
                LOGGER.info(String.format("Code: %d. Reason: %s.", result.getStatusLine().getStatusCode(), result.getStatusLine().getReasonPhrase()));
                return;
            }

            BufferedReader r = new BufferedReader(new InputStreamReader(result.getEntity().getContent()));
            char[] buff = new char[256];
            StringBuilder sb = new StringBuilder();
            int numRead;
            while ((numRead = r.read(buff)) > 0) {
                sb.append(buff, 0, numRead);
            }

            LOGGER.info(String.format("Response: %s.", sb.toString()));

            sendResponse(_receiver, true, result.getStatusLine().getReasonPhrase(), sb.toString());

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            sendResponse(_receiver, false, getResources().getString(R.string.error_network_error), null);
        }
    }
    
    /**
     * 
     * @param _receiver
     * @param success
     * @param error
     * @param body
     */
    private void sendResponse(ResultReceiver _receiver, boolean success, String error, String body) {
        Bundle resultData = new Bundle();
        if (!TextUtils.isEmpty(error)) {
            resultData.putString(RESPONSE_KEY_ERROR, error);
        }
        if (!TextUtils.isEmpty(body)) {
            resultData.putString(RESPONSE_KEY_DATA, body);
        }
        _receiver.send(success ? 1 : 0, resultData );
    }
}
