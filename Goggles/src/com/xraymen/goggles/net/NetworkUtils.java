package com.xraymen.goggles.net;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import com.xraymen.goggles.db.Message;
import com.xraymen.goggles.db.Profile;
import com.xraymen.goggles.db.Settings;

import android.location.Location;
import android.text.TextUtils;

/**
 * Network Utilities to build URLs and POST bodies.
 * 
 * @author amitlissack
 * 
 */
public class NetworkUtils {

    private static final Logger LOGGER = Logger.getLogger(NetworkUtils.class.getName());

    public static final String KEY_RATING = "rating";
    public static final String KEY_MESSAGEID = "messageId";
    public static final String KEY_WELCOME = "welcome";
    public static final String KEY_ALTITUDE = "altitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_VIEW = "view";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_OWNERID = "ownerId";
    public static final String KEY_CREATED = "created";
    public static final String KEY_EXPIRATION = "expiration";
    public static final String KEY_STATUS = "status";
    public static final String KEY_CODE = "code";
    public static final String KEY_TEXT = "text";
    public static final String KEY_PROFILE = "profile";
    public static final String KEY_MESSAGES = "messages";
    public static final String KEY_PROFILES = "profiles";
    public static final String KEY_IMAGEURLFULL = "imageUrlFull";
    public static final String KEY_IMAGEURLTHUMB = "imageUrlThumb";
    public static final String KEY_ID = "id";
    public static final String KEY_SUPPORTSFEEDBACK="supportsFeedback";
    public static final String KEY_RATINGDISLIKES="ratingDislikes";
    public static final String KEY_TYPE="type";
    public static final String KEY_RATINGLIKES="ratingLikes";
    public static final String KEY_RATINGUSER="ratingUser";
    public static final String KEY_FLAGGEDUSER="flaggedUser";
    public static final String KEY_COLOR = "color";
    public static final String KEY_RATINGSUM = "ratingSum";
    public static final String KEY_ERROR = "error";
    public static final String KEY_VERSION = "version";
    public static final String KEY_SETTINGS = "settings";
    public static final String KEY_QUERYINTERVALMAX = "queryIntervalMax";
    public static final String KEY_NAME = "name";
    public static final String KEY_QUERYINTERVALMIN = "queryIntervalMin";
    public static final String KEY_RADIUSHORIZONTAL = "radiusHorizontal";
    public static final String KEY_MESSAGESPERQUERY = "messagesPerQueryMax";
    public static final String KEY_RADIUSVERTICAL = "radiusVertical";
    public static final String KEY_DEVICEID = "device";
    public static final String KEY_SERVER = "server";

    public static final String VALUE_OK = "okay";
    public static final String VALUE_FAIL = "fail";

    private static final boolean PRODUCTION = true;
    private static final boolean SECURE = false;
    
    public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    
    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    /**
     * 
     * @return
     */
    private static final String getHost() {

        if (PRODUCTION) {
            return (SECURE) ? "http://goggles.xraymen.com"
                    : "http://goggles.xraymen.com";
        } else {
            return (SECURE) ? "http://ghosts.furrylab.com"
                    : "http://ghosts.furrylab.com";
        }
    }

    /**
     * 
     * @return
     */
    public static final String getUrlGetSettings() {
        return String.format("%s/settings/get/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlGetMessages() {
        return String.format("%s/messages/get/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlPostMessage() {
        return String.format("%s/messages/put/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlRateMessage() {
        return String.format("%s/messages/rate/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlFlagMessage() {
        return String.format("%s/messages/flag/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlLoginUser() {
        return String.format("%s/profile/login/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlLogoutUser() {
        return String.format("%s/profile/logout/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlRegisterUser() {
        return String.format("%s/profile/register/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlSendFeedback() {
        return String.format("%s/support/feedback/json", getHost());
    }
    
    /**
     * 
     * @return
     */
    public static final String getUrlDeleteMessage() {
        return String.format("%s/messages/delete/json", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlPrivacyPolicy() {
        return String.format("%s/static/html/privacy.html", getHost());
    }

    /**
     * 
     * @return
     */
    public static final String getUrlTermsAndConditions() {
        return String.format("%s/static/html/terms.html", getHost());
    }

    /**
     * 
     * @param username
     * @param password
     * @return
     */
    public static String buildLoginRequest(String username, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_USERNAME, username);
            jsonObject.put(KEY_PASSWORD, password);
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 
     * @param username
     * @return
     */
    public static String buildLogoutRequest(String username) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_USERNAME, username);
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;

    }

    /**
     * 
     * @param username
     * @param emailAddress
     * @param password
     * @return
     */
    public static String buildRegisterRequest(String username,
            String emailAddress, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_USERNAME, username);
            jsonObject.put(KEY_PASSWORD, password);
            jsonObject.put(KEY_EMAIL, emailAddress);
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 
     * @param latitude
     * @param longitude
     * @param altitude
     * @param welcomeState
     * @param view
     * @return
     */
    public static String buildGetMessagesRequest(double latitude,
            double longitude, double altitude, boolean welcomeState, String view) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_VIEW, view == null ? "radial" : view);
            jsonObject.put(KEY_LATITUDE, latitude);
            jsonObject.put(KEY_LONGITUDE, longitude);
            jsonObject.put(KEY_ALTITUDE, altitude);
            if (welcomeState) {
                jsonObject.put(KEY_WELCOME, true);
            }
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 
     * @param messageId
     * @param rating
     * @return
     */
    public static String buildRateMessageRequest(long messageId, int rating) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_MESSAGEID, messageId);
            jsonObject.put(KEY_RATING, rating);
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 
     * @param messageId
     * @return
     */
    public static String buildFlagMessageRequest(long messageId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_MESSAGEID, messageId);
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * 
     * @param ownerId
     * @param text
     * @param latitude
     * @param longitude
     * @param altitude
     * @param created
     * @param expiration
     * @return
     */
    public static String buildPostMessageRequest(String ownerId, String text, double latitude, double longitude, double altitude, Date created, Date expiration) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_OWNERID, ownerId);
            jsonObject.put(KEY_TEXT, text);
            jsonObject.put(KEY_LATITUDE, latitude);
            jsonObject.put(KEY_LONGITUDE, longitude);
            jsonObject.put(KEY_ALTITUDE, altitude);
            if (created == null) {
                created = new Date();
            }
            jsonObject.put(KEY_CREATED, DATE_FORMAT.format(created));
            if (expiration != null && !expiration.equals(created)) {
                jsonObject.put(KEY_EXPIRATION, (expiration.getTime() - created.getTime()) / 1000);
            }
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * 
     * @param ownerId
     * @param text
     * @param location
     * @param created
     * @param expiration
     * @return
     */
    public static String buildPostMessageRequest(String ownerId, String text, Location location, Date created, Date expiration) {
        return buildPostMessageRequest(ownerId, text, location.getLatitude(), location.getLongitude(), location.getAltitude(), created, expiration);
    }
    
    /**
     * 
     * @param version
     * @return
     */
    public static String buildGetSettingsRequest(String version) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(version)) {
                jsonObject.put(KEY_VERSION, version);
            }
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * 
     * @param ownerId
     * @param text
     * @return
     */
    public static String buildSendFeedbackRequest(String ownerId, String text) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_TEXT, text);
            jsonObject.put(KEY_OWNERID, ownerId);
            String device = android.os.Build.SERIAL;
            jsonObject.put(KEY_DEVICEID, device);
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * 
     * @param messageId
     * @return
     */
    public static String buildDeleteMessageRequest(long messageId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_MESSAGEID, messageId);
            return jsonObject.toString();
        } catch (JSONException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * Initialize from JSON object
     * @param o
     * @throws JSONException
     * @throws ParseException
     */
    public static Message messageFromJson(JSONObject o) throws JSONException, ParseException {
        Message message = new Message();
        return mergeMessageFromJson(o, message);
    }
    
    /**
     * 
     * @param o
     * @param message
     * @return
     * @throws JSONException
     * @throws ParseException
     */
    public static Message mergeMessageFromJson(final JSONObject o, Message message) throws JSONException, ParseException {
        
        if (o.has(NetworkUtils.KEY_ID)) message.setGlobalId(o.getInt(NetworkUtils.KEY_ID));
        if (o.has(NetworkUtils.KEY_CREATED)) message.setCreated(NetworkUtils.DATE_FORMAT.parse(o.getString(NetworkUtils.KEY_CREATED)));
        if (o.has(NetworkUtils.KEY_TEXT)) message.setText(o.getString(NetworkUtils.KEY_TEXT));
        if (o.has(NetworkUtils.KEY_ALTITUDE)) message.setAltitude(o.getDouble(NetworkUtils.KEY_ALTITUDE));
        if (o.has(NetworkUtils.KEY_LONGITUDE)) message.setLongitude(o.getDouble(NetworkUtils.KEY_LONGITUDE));
        if (o.has(NetworkUtils.KEY_SUPPORTSFEEDBACK)) message.setSupportsFeedback(o.getBoolean(NetworkUtils.KEY_SUPPORTSFEEDBACK));
        if (o.has(NetworkUtils.KEY_EXPIRATION)) message.setExpiration(NetworkUtils.DATE_FORMAT.parse(o.getString(NetworkUtils.KEY_EXPIRATION)));
        if (o.has(NetworkUtils.KEY_RATINGDISLIKES)) message.setRatingDislikes(o.getInt(NetworkUtils.KEY_RATINGDISLIKES));
        if (o.has(NetworkUtils.KEY_LATITUDE)) message.setLatitude(o.getDouble(NetworkUtils.KEY_LATITUDE));
        if (o.has(NetworkUtils.KEY_OWNERID)) message.setOwnerId(o.getString(NetworkUtils.KEY_OWNERID));
        if (o.has(NetworkUtils.KEY_TYPE)) message.setType(o.getString(NetworkUtils.KEY_TYPE));
        if (o.has(NetworkUtils.KEY_RATINGLIKES)) message.setRatingLikes(o.getInt(NetworkUtils.KEY_RATINGLIKES));
        if (o.has(NetworkUtils.KEY_RATINGUSER)) message.setRatingUser(o.getInt(NetworkUtils.KEY_RATINGUSER));
        if (o.has(NetworkUtils.KEY_FLAGGEDUSER)) message.setFlaggedUser(o.getBoolean(NetworkUtils.KEY_FLAGGEDUSER));
        if (o.has(NetworkUtils.KEY_IMAGEURLFULL)) message.setImageUrlFull(o.getString(NetworkUtils.KEY_IMAGEURLFULL));
        if (o.has(NetworkUtils.KEY_IMAGEURLTHUMB)) message.setImageUrlThumb(o.getString(NetworkUtils.KEY_IMAGEURLTHUMB));
        message.setRatingSum(message.getRatingLikes() - message.getRatingDislikes());
        
        return message;
    }
    
    
    /**
     * Initialize from JSON Object
     * @param o
     * @throws ParseException
     * @throws JSONException
     */
    public static Profile profileFromJson(JSONObject o) throws ParseException, JSONException {
        Profile profile = new Profile();

        if (o.has(NetworkUtils.KEY_CREATED)) profile.setCreated(NetworkUtils.DATE_FORMAT.parse(o.getString(NetworkUtils.KEY_CREATED)));
        if (o.has(NetworkUtils.KEY_EMAIL)) profile.setEmail(o.getString(NetworkUtils.KEY_EMAIL));
        if (o.has(NetworkUtils.KEY_USERNAME)) profile.setUsername(o.getString(NetworkUtils.KEY_USERNAME));
        if (o.has(NetworkUtils.KEY_STATUS)) profile.setStatus(o.getString(NetworkUtils.KEY_STATUS));
        if (o.has(NetworkUtils.KEY_IMAGEURLFULL)) profile.setImageUrlFull(o.getString(NetworkUtils.KEY_IMAGEURLFULL));
        if (o.has(NetworkUtils.KEY_IMAGEURLTHUMB)) profile.setImageUrlThumb(o.getString(NetworkUtils.KEY_IMAGEURLTHUMB));

        return profile;
    }
    
    /**
     * 
     * @param o
     * @return
     * @throws JSONException
     */
    public static Settings settingsFromJson(JSONObject o) throws JSONException {
        Settings settings = new Settings();
        
        if (o.has(KEY_NAME)) settings.setName(o.getString(KEY_NAME));
        if (o.has(KEY_QUERYINTERVALMIN)) settings.setQueryIntervalMin(o.getInt(KEY_QUERYINTERVALMIN));
        if (o.has(KEY_QUERYINTERVALMAX)) settings.setQueryIntervalMax(o.getInt(KEY_QUERYINTERVALMAX));
        if (o.has(KEY_RADIUSHORIZONTAL)) settings.setRadiusHorizontal(o.getDouble(KEY_RADIUSHORIZONTAL));
        if (o.has(KEY_RADIUSVERTICAL)) settings.setRadiusVertical(o.getDouble(KEY_RADIUSVERTICAL));
        if (o.has(KEY_MESSAGESPERQUERY)) settings.setMessagesPerQuery(o.getInt(NetworkUtils.KEY_MESSAGESPERQUERY));
        
        return settings;
    }
}
