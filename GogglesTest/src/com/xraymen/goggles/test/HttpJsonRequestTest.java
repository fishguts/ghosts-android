package com.xraymen.goggles.test;

import junit.framework.TestCase;

import org.json.JSONException;
import org.json.JSONObject;

import com.xraymen.goggles.net.HttpJsonRequest;
import com.xraymen.goggles.net.HttpRequestService;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.test.mock.MockContext;
import android.util.Log;

public class HttpJsonRequestTest extends TestCase {

    private static final String TAG = HttpJsonRequestTest.class.getName();

    /**
     * 
     */
    public void testGood() {
        final HttpRequestServiceContext context = new HttpRequestServiceContext() {
            @Override
            protected int getResultCode() {
                return 1;
            }
            
            @Override
            protected String getData() {
                return "{\"key\":\"value\", \"status\":{\"code\":\"okay\"}}";
            }
        };
        
        HttpJsonRequest j = new HttpJsonRequest(context, "", new HttpJsonRequest.JsonResponseListener() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    assertEquals("value", response.getString("key"));
                } catch (JSONException e) {
                    fail();
                }
            }

            @Override
            public void onFailure(String status, String text) {
                fail();
            }
        });
        j.execute();
    }
    
    /**
     * 
     */
    public void testFailResult() {
        final HttpRequestServiceContext context = new HttpRequestServiceContext() {
            @Override
            protected int getResultCode() {
                return 0;
            }
            
            @Override
            protected String getError() {
                return "failure!";
            }
        };
        
        HttpJsonRequest j = new HttpJsonRequest(context, "", new HttpJsonRequest.JsonResponseListener() {
            @Override
            public void onSuccess(JSONObject response) {
                fail();
            }

            @Override
            public void onFailure(String status, String text) {
                assertEquals("failure!", text);
            }
        });
        j.execute();
    }
    
    /**
     * 
     */
    public void testGoodJsonError() {
        final HttpRequestServiceContext context = new HttpRequestServiceContext() {
            @Override
            protected int getResultCode() {
                return 1;
            }
            
            @Override
            protected String getData() {
                return "{\"status\":{\"code\":\"auth\", \"text\":\"smelly\"}}";
            }
        };
        
        HttpJsonRequest j = new HttpJsonRequest(context, "", new HttpJsonRequest.JsonResponseListener() {
            @Override
            public void onSuccess(JSONObject response) {
                fail();
            }

            @Override
            public void onFailure(String status, String text) {
                assertEquals("auth", status);
                assertEquals("smelly", text);
            }
        });
        j.execute();
    }
    
    /**
     * 
     */
    public void testBadJson() {
        final HttpRequestServiceContext context = new HttpRequestServiceContext() {
            @Override
            protected int getResultCode() {
                return 1;
            }
            
            @Override
            protected String getData() {
                return "{this is some bad json";
            }
        };
        
        HttpJsonRequest j = new HttpJsonRequest(context, "", new HttpJsonRequest.JsonResponseListener() {
            @Override
            public void onSuccess(JSONObject response) {
                fail();
            }

            @Override
            public void onFailure(String status, String text) {
                Log.d(TAG, text);
            }
        });
        j.execute();
    }
    
    /**
     * 
     */
    public void testGoodJsonNoStatus() {
        final HttpRequestServiceContext context = new HttpRequestServiceContext() {
            @Override
            protected int getResultCode() {
                return 1;
            }
            
            @Override
            protected String getData() {
                return "{\"statu\":{\"code\":1}}";
            }
        };
        
        HttpJsonRequest j = new HttpJsonRequest(context, "", new HttpJsonRequest.JsonResponseListener() {
            @Override
            public void onSuccess(JSONObject response) {
                fail();
            }

            @Override
            public void onFailure(String status, String text) {
                Log.d(TAG, text);
            }
        });
        j.execute();
    }
    
    /**
     * 
     */
    public void testGoodJsonInvalidCode() {
        final HttpRequestServiceContext context = new HttpRequestServiceContext() {
            @Override
            protected int getResultCode() {
                return 1;
            }
            
            @Override
            protected String getData() {
                return "{\"status\":{\"code\":1}}";
            }
        };
        
        HttpJsonRequest j = new HttpJsonRequest(context, "", new HttpJsonRequest.JsonResponseListener() {
            @Override
            public void onSuccess(JSONObject response) {
                fail();
            }

            @Override
            public void onFailure(String status, String text) {
                Log.d(TAG, text);
            }
        });
        j.execute();
    }
    
    /**
     * 
     */
    public void testGoodJsonNoErrorText() {
        final HttpRequestServiceContext context = new HttpRequestServiceContext() {
            @Override
            protected int getResultCode() {
                return 1;
            }
            
            @Override
            protected String getData() {
                return "{\"status\":{\"code\":\"fail\"}}";
            }
        };
        
        HttpJsonRequest j = new HttpJsonRequest(context, "", new HttpJsonRequest.JsonResponseListener() {
            @Override
            public void onSuccess(JSONObject response) {
                fail();
            }

            @Override
            public void onFailure(String status, String text) {
                Log.d(TAG, text);
            }
        });
        j.execute();
    }

    /**
     * 
     * @author amitlissack
     * 
     */
    private static class HttpRequestServiceContext extends MockContext {

        @Override
        public String getPackageName() {
            return "com.xraymen.goggles.test";
        }

        @Override
        public ComponentName startService(Intent service) {
//            Uri uri = service.getData();
            ResultReceiver r = (ResultReceiver) service.getExtras().getParcelable(HttpRequestService.EXTRA_RECEIVER);
//            String body = service.getExtras().getString(HttpRequestService.EXTRA_DATA);
//            int verb = service.getExtras().getInt(HttpRequestService.EXTRA_VERB);
            
            Bundle resultData = new Bundle();
            
            resultData.putString(HttpRequestService.RESPONSE_KEY_ERROR, getError());
            resultData.putString(HttpRequestService.RESPONSE_KEY_DATA, getData());
            r.send(getResultCode(), resultData);

            return new ComponentName("", "");
        }

        protected String getData() {
            return "";
        }

        protected String getError() {
            return "";
        }

        protected int getResultCode() {
            return 0;
        }

        @Override
        public boolean stopService(Intent service) {
            return true;
        }
    }
}
