package com.xraymen.goggles.test;

import java.util.Date;

import com.xraymen.goggles.LocationService;
import com.xraymen.goggles.LocationService.LocationServiceBinder;

import android.content.Intent;
import android.location.Location;
import android.os.SystemClock;
import android.test.ServiceTestCase;
import android.text.TextUtils;
import android.util.Log;

public class LocationServiceTest extends ServiceTestCase<LocationService> {
    
    private final static String TAG = LocationServiceTest.class.getName();
    
    private LocationServiceBinder _binder;
        
    public LocationServiceTest(Class<LocationService> serviceClass) {
        super(serviceClass);
    }
    
    public LocationServiceTest() {
        super(LocationService.class);
    }
    
    @Override
    protected void setUp() throws Exception {
        // TODO Auto-generated method stub
        super.setUp();
        
        Log.d(TAG, "setUp");
        
        _binder = (LocationServiceBinder)bindService(new Intent(getContext(), LocationService.class));
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testAccuracy() {

        //Far away but way much less accurate
        sendLocation(null, 37.81018002, -122.2551578,  0, 1000);
        sendLocation(null, 37.91018002, -122.3551578,  0, 1300);
        assertNotNull(_binder.getCurrentLocation());
        assertEquals(_binder.getCurrentLocation().getAccuracy(), 1000.0, 0);
        
        //Far away but slightly less accurate but different provider
        sendLocation("eggprovider", 37.91018002, -122.3551578,  0, 1030);
        assertEquals(_binder.getCurrentLocation().getAccuracy(), 1000.0, 0);
        
        //Far away but slightly less accurate
        sendLocation(null, 37.91018002, -122.3551578,  0, 1030);
        assertNotNull(_binder.getCurrentLocation());
        assertEquals(_binder.getCurrentLocation().getAccuracy(), 1030.0, 0);
        
        //Close but way more accurate
        sendLocation(null, 37.91018003, -122.3551579,  0, 500);
        assertNotNull(_binder.getCurrentLocation());
        assertEquals(_binder.getCurrentLocation().getAccuracy(), 500.0, 0);

    }
    
    public void testAltitude() {
        sendLocation(null, 37.81018002, -122.2551578, 0, 1);
        assertEquals(_binder.getCurrentLocation().getAltitude(), 0f, 0);
        sendLocation(null, 37.81018002, -122.2551578, 30, 1);
        assertEquals(_binder.getCurrentLocation().getAltitude(), 0f, 0);
        sendLocation(null, 37.81018002, -122.2551578, 50, 1);
        assertEquals(_binder.getCurrentLocation().getAltitude(), 0f, 0);
        //Finally pass the altitude threshold
        sendLocation(null, 37.81018002, -122.2551578, 90, 1);
        assertEquals(_binder.getCurrentLocation().getAltitude(), 90f, 0);
        
        //Now below
        sendLocation(null, 37.81018002, -122.2551578, -90, 1);
        assertEquals(_binder.getCurrentLocation().getAltitude(), -90f, 0);

    }
    
    public void testDistance() {
        sendLocation(null, 37.81018002, -122.2551578, 0, 1);
        assertEquals(_binder.getCurrentLocation().getLongitude(), -122.2551578, 0);
        sendLocation(null, 37.81038002, -122.2553578, 0, 1);
        assertEquals(_binder.getCurrentLocation().getLongitude(), -122.2551578, 0);
        //Pass the distance threshold
        sendLocation(null, 37.81078002, -122.2557578, 0, 1);
        assertEquals(_binder.getCurrentLocation().getLongitude(), -122.2557578, 0);
        sendLocation(null, 37.81098002, -122.2559578, 0, 1);
        assertEquals(_binder.getCurrentLocation().getLongitude(), -122.2557578, 0);
    }
    
    public void testWithTime() {
        final long startTime = new Date().getTime();
        final long minute = 60 * 1000;
        
        sendLocation(null, 37.81018002, -122.2551578, 0, 200, startTime);
        assertEquals(_binder.getCurrentLocation().getLongitude(), -122.2551578, 0);
        
        //Much more accurate but much older. Ignored.
        sendLocation(null, 37.91018002, -122.9551578, 0, 1, startTime - (minute * 10));
        assertEquals(_binder.getCurrentLocation().getLongitude(), -122.2551578, 0);
        
        //Much less accurate but newer. Not ignored 
        sendLocation(null, 37.91018002, -122.9551578, 0, 1000, startTime + (minute * 10));
        assertEquals(_binder.getCurrentLocation().getLongitude(), -122.9551578, 0);
    }
    
    /**
     * @param provider
     * @param lat
     * @param lon
     * @param alt
     * @param accuracy
     * @param time
     */
    private void sendLocation(String provider, double lat, double lon, double alt, float accuracy, long time) {

        Location l = new Location(TextUtils.isEmpty(provider) ? "" : provider);
        l.setAccuracy(accuracy);
        l.setLatitude(lat);
        l.setLongitude(lon);
        l.setAltitude(alt);
        l.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        l.setTime(time);
        
        getService().onLocationChanged(l);
    }
    
    /**
     * 
     * @param provider
     * @param lat
     * @param lon
     * @param alt
     * @param accuracy
     */
    private void sendLocation(String provider, double lat, double lon, double alt, float accuracy) {
        sendLocation(provider, lat, lon, alt, accuracy, new Date().getTime());
    }
}
